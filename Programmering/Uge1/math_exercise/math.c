#include<stdio.h>
#include<math.h>
#include<complex.h>
int main()
{
	double x = 5;
	double y = tgamma(x);
	double z = j1(x);
	double a = -2;
	double complex b = csqrt(a);
	double complex d = cpow(M_E,I);
	double complex f = cpow(M_E,I*M_PI);
	double complex h = cpow(I,M_E);


	printf("--- First Exercise: Evaluate functions ---\n");
	printf("%s%g%s%g\n","gamma(",x,") = ",y);
	printf("%s%g%s%g\n","j1(",x,") = ",z);
	printf("sqrt(%g) = %g + %g *i\n",a,creal(b),cimag(b));
	printf("exp(i) = %g + %g * i\n",creal(d), cimag(d));
	printf("exp(pi*i) = %g + %g * i\n",creal(f), cimag(f));
	printf("i^(e) = %g + %g * i\n\n",creal(h), cimag(h));

	printf("--- Second Exercise: Data Type Precision ---\n");
	float l = 0.1111111111111111111111111111;
	double m = 0.1111111111111111111111111111;
	long double n = 0.1111111111111111111111111111L;

	printf("float: %.25g\n",l);
	printf("double: %.25lg\n",m);
	printf("Long double: %.25Lg\n",n);
	return 0;
}
