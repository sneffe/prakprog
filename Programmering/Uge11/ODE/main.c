#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<math.h>

void rkstep23(double t, double h, gsl_vector* yt, void f(double t, gsl_vector* y, gsl_vector* dydt), gsl_vector* yth, gsl_vector* err);
void f_exp(double t, gsl_vector* y, gsl_vector* dydt);
void f_cos(double t, gsl_vector* y, gsl_vector* dydt);
void driver(double* t, double b, double* h, gsl_vector* yt, double acc, double eps, 
		void rkstep12(double t, double h, gsl_vector* yt, void f(double t,gsl_vector* y, gsl_vector* dydt),
		gsl_vector* yth, gsl_vector* err),void f(double t, gsl_vector* y, gsl_vector* dydt));
void driver_with_path(double* t, double b, double* h, gsl_vector* yt, double acc, double eps, 
		void rkstep23(double t, double h, gsl_vector* yt, void f(double t,gsl_vector* y, gsl_vector* dydt),
		gsl_vector* yth, gsl_vector* err),void f(double t, gsl_vector* y, gsl_vector* dydt), gsl_matrix* Y, int* num);

int main(){
	int n = 1;
	int m = 2;
	int number_of_points_stored = 1000;
	double h_value = 1e-4;
	double t_value = 0;
	
	double* h; // start step sizeof
	double* t; // start time
	
	h = &h_value; // assign value to pointer
	t = &t_value; // assign value to pointer

	double b = 5;
	double acc = 1e-2;
	double eps = 1e-2; 
	
	gsl_vector* yt = gsl_vector_alloc(n);
	gsl_vector* yt2 = gsl_vector_alloc(m);
	gsl_vector_set(yt,0,1); // set y(t = 0) = 1; as for the exponential function

	
	printf("--- Ordinary Differential Equations ---\n\n");
	printf("Exercise A\n");
	printf("Differential Equation: dfdx = f(x)\n");
	printf("a = %g\n", t_value);
	printf("b = %g\n", b);
	printf("eps = %g\n",eps);
	printf("acc = %g\n",acc);
	printf("h_initial = %g\n\n",*h);
	driver(t,b,h,yt,acc,eps,rkstep23,f_exp);
	printf("Call ODE driver... \n");
	printf("t_end = %g\n",*t);
	printf("h_end = %g\n",*h);
	printf("y(b) = %g\n", gsl_vector_get(yt,0));
	printf("exp(b) = %g\n",exp(*t));
	printf("Yay! ^__^\n\n");

	h_value = 1e-4;
	t_value = 0;
	b = M_PI;
	h = &h_value; // assign value to pointer
	t = &t_value; // assign value to pointer
	gsl_vector_set(yt2,0,1); // starting y point
	gsl_vector_set(yt2,1,0); // starting slope

	printf("Differential Equation: dfdx = -f(x)\n");
	printf("a = %g\n", t_value);
	printf("b = %g\n", b);
	printf("eps = %g\n",eps);
	printf("acc = %g\n",acc);
	printf("h_initial = %g\n\n",*h);
	driver(t,b,h,yt2,acc,eps,rkstep23,f_cos);
	printf("Call ODE driver... \n");
	printf("t_end = %g\n",*t);
	printf("h_end = %g\n",*h);
	printf("y(b) = %g\n", gsl_vector_get(yt2,0));
	printf("cos(b) = %g\n",cos(*t));
	printf("Yay! ^__^\n\n");
	

	
	printf("Exercise B\n");
	printf("Store the path of the ODE solver\n");
	
	h_value = 1e-4;
	t_value = 0;
	b = M_PI;
	h = &h_value; // assign value to pointer
	t = &t_value; // assign value to pointer
	gsl_vector_set(yt2,0,1); // starting y point
	gsl_vector_set(yt2,1,0); // starting slope
	int* num = &number_of_points_stored;
	
	printf("Differential Equation: dfdx = -f(x)\n");
	printf("a = %g\n", t_value);
	printf("b = %g\n", b);
	printf("eps = %g\n",eps);
	printf("acc = %g\n",acc);
	printf("h_initial = %g\n\n",*h);
	printf("Plot.svg now contains the path of the ODE solver along with the first derivative\n");
	printf("From plot.svg we see that the resulting function from the ODE solver agrees with the expected result based on the analytical solution to the differential equation\n");
	
	gsl_matrix* Y = gsl_matrix_alloc(*num, m+1);
	driver_with_path(t,b,h,yt2,acc,eps,rkstep23,f_cos,Y,num);

	FILE * datastream; //Open a data file and print the data points into it
	datastream = fopen("data.txt", "w+");
			for(int i = 0; i < *num; i++){
				fprintf(datastream,"%g \t %g \t %g \t %g \t %g \n",gsl_matrix_get(Y,i,0), gsl_matrix_get(Y,i,1), gsl_matrix_get(Y,i,2),cos(gsl_matrix_get(Y,i,0)), -sin(gsl_matrix_get(Y,i,0)));
			}
	fclose(datastream);
	
	gsl_matrix_free(Y);
	gsl_vector_free(yt);
	gsl_vector_free(yt2);
	return 0;
}

void f_exp(double t, gsl_vector* y, gsl_vector* dydt){
	// I have chosen a first order ODE for the exponential function
	double y0 = gsl_vector_get(y,0);
	gsl_vector_set(dydt,0,y0); // dydt = y
}

void f_cos(double t, gsl_vector* y, gsl_vector* dydt){
	// I have chosen a first order ODE for the exponential function
	double y0 = gsl_vector_get(y,0);
	gsl_vector_set(dydt,0,gsl_vector_get(y,1));
	gsl_vector_set(dydt,1,-y0);
	
}

