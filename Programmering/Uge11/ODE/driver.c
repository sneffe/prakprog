#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<math.h>

void f(double t, gsl_vector* y, gsl_vector* dydt);
void rkstep23(double t, double h, gsl_vector* yt, void f(double t, gsl_vector* y, gsl_vector* dydt), gsl_vector* yth, gsl_vector* err);

int driver(double* t, double b, double* h, gsl_vector* yt, double acc, double eps, 
		void rkstep23(double t, double h, gsl_vector* yt, void f(double t,gsl_vector* y, gsl_vector* dydt),
		gsl_vector* yth, gsl_vector* err),void f(double t, gsl_vector* y, gsl_vector* dydt)){

	int n = yt->size;
	
	if (*t == b) return 0;
	if (fabs(*t == *h) > fabs(b)) {*h = b - *t;}
	
	gsl_vector* yh = gsl_vector_alloc(n);
    gsl_vector* err = gsl_vector_alloc(n);

	int iter = 0;
	double err_norm;
	double y_norm;
	double tau;
	double a = *t;
	
	do{
		iter++;
		rkstep23(*t, *h, yt, f, yh, err);
        err_norm = gsl_blas_dnrm2(err);
		y_norm = gsl_blas_dnrm2(yt);
		
		tau = (eps*y_norm + acc)*sqrt(*h/(b-a));
		
		if(tau > err_norm){ 
			gsl_vector_memcpy(yt,yh);
			*t = *t + *h;
		}
		
		*h *= pow(tau/err_norm,0.25) * 0.95;
		
		if(fabs(*t + *h) > fabs(b)){*h = b - *t;} 
	} while(iter < 1e6 && fabs(*t - b) > 1e-12);
	
	if(iter == 1e6){
        fprintf(stderr, "Error in driver: ODE did not converge after %i cycles\n", iter);
        return -1;
    }
	
	return 0;
	
	gsl_vector_free(yh);
	gsl_vector_free(err);
}	

int driver_with_path(double* t, double b, double* h, gsl_vector* yt, double acc, double eps, 
		void rkstep23(double t, double h, gsl_vector* yt, void f(double t,gsl_vector* y, gsl_vector* dydt),
		gsl_vector* yth, gsl_vector* err),void f(double t, gsl_vector* y, gsl_vector* dydt), gsl_matrix* Y, int* num){
	
	 if(yt->size != (Y->size2)-1){
        fprintf(stderr, "Error in driver_with_path: Dimention mismatch.");
        return -1;
    }
	
	int n = yt -> size;
	gsl_vector* yh = gsl_vector_alloc(n);
    gsl_vector* err = gsl_vector_alloc(n);

	int iter = 0;
	*num = 0;
	double err_norm;
	double y_norm;
	double tau;
	double a = *t;
	
	do{
		iter++;
		rkstep23(*t, *h, yt, f, yh, err);
        err_norm = gsl_blas_dnrm2(err);
		y_norm = gsl_blas_dnrm2(yt);
		
		tau = (eps*y_norm + acc)*sqrt(*h/(b-a));
		
		if(tau > err_norm){ 
			gsl_vector_memcpy(yt,yh);
			*t = *t + *h;
			
            if (*num < Y->size1){
                gsl_matrix_set(Y, *num, 0, *t);  // Set the t value in the first row in Y
                for (int i = 0; i < (yh->size); ++i) {
                    gsl_matrix_set(Y, *num, i+1, gsl_vector_get(yh, i)); // Set y-value and derivatives in the other rows of Y
                }
			}
			(*num)++;
		}
		
		*h *= pow(tau/err_norm,0.25) * 0.95;
		
		if(fabs(*t + *h) > fabs(b)){*h = b - *t;} 
	} while(iter < 1e6 && fabs(*t - b) > 1e-12);
	
	if(iter == 1e6){
        fprintf(stderr, "Error in driver: ODE did not converge after %i cycles\n", iter);
        return -1;
    }
	
	return 0;
	
	gsl_vector_free(yh);
	gsl_vector_free(err);
}	
