#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<assert.h>

int calls; // Global variable

double adapt(double f(double x),double a,double b,double acc,double eps);
double clenshaw_curtis(double f(double), double a, double b, double acc, double eps);

double g1(double x){
		calls++;
		return sqrt(x);
	}
	
double g2(double x){
		calls++;
		return 1/sqrt(x);
	}

double g3(double x){
		calls++;
		return log(x)/sqrt(x);
	}	

double g4(double x){
	calls++;
	return 4*sqrt(1-pow((1-x),2));
}

int main(){
	
	double a = 0;
	double b = 1; 
	double acc = 1e-4;
	double eps = 1e-4;
	
	printf("--- Numerical Integration --\n\n");
	printf("Exercise A: Recursive Adaptive Integrator\n");
	printf("eps = %g\n",eps);
	printf("acc = %g\n\n",acc);

	printf("a = %g\n",a);
	printf("b = %g\n\n",b);
	
	calls = 0; // set global variable value
	printf("f(x) = sqrt(x)\n");
	printf("main: calling adapt\n");
	double Q = adapt(g1,a,b,acc,eps);
	printf("Q = %g,\t calls = %i\n", Q, calls);
	printf("Expected result: Q = 0.6666\n\n");
	
	calls = 0;
	printf("f(x) = 1/sqrt(x)\n");
	printf("main: calling adapt\n");
	Q = adapt(g2,a,b,acc,eps);
	printf("Q = %g,\t calls = %i\n", Q, calls);
	printf("Expected result: Q = 2\n\n");
	
	calls = 0;
	printf("f(x) = ln(x)/sqrt(x)\n");
	printf("main: calling adapt\n");
	Q = adapt(g3,a,b,acc,eps);
	printf("Q = %g,\t calls = %i\n", Q, calls);
	printf("Expected result: Q = -4\n\n");
	
	calls = 0;
	printf("f(x) = 4*sqrt(1-(1-x)^2))\n");
	printf("main: calling adapt\n");
	Q = adapt(g4,a,b,acc,eps);
	printf("Q = %g,\t calls = %i\n", Q, calls);
	printf("Expected result: Q = 3.14159265\n\n");
	printf("SUCCESS\n\n");
	
	printf("Exercise B: Clenshaw–Curtis variable transformation\n\n");
	Q = clenshaw_curtis(g3,a,b,acc,eps);
	printf("Q = %g,\t calls = %i\n", Q, calls);
	printf("Expected result: Q = -4\n\n");
	printf("SUCCESS\n\n");
	
	return 0;
}
