#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<math.h>
#define RND ((double)rand()/RAND_MAX)  // Needed to get random complex entry in Hermitian matrix
#define FMT "%7.3f"

void column_from_matrix(gsl_vector* v,gsl_matrix* A, int n){ // returns vector v which is the n'th column in A
	int N = A->size1;
	
	for(int i=0;i<N;i++){
		gsl_vector_set(v,i,gsl_matrix_get(A,i,n));
	}
}

void matrix_print(gsl_matrix* A){   // prints elements of gsl matrix of size (size1, size2)
	for(int r=0;r<A->size1;r++){
		for(int c=0;c<A->size2;c++) printf(FMT,gsl_matrix_get(A,r,c));
		printf("\n");}
}

void vector_print(gsl_vector* v){   // prints elements of gsl matrix of size (size1, size2)
	for(int r=0;r<v->size;r++){
		printf(FMT,gsl_vector_get(v,r));
		printf("\n");}
}

double dot(gsl_vector* a, gsl_vector* b){
	double result;
	if(a->size != b->size){
		printf("vectors must be same length\n");
	}
	for(int i=0;i < a->size; i++){
		result += gsl_vector_get(a,i)*gsl_vector_get(b,i);
	}
	return result;
}

int main(){
	int N = 6; // Size of A
	int n = 6; // Krylov subspace dimension
	
	printf("Exam Exercise 11: Lanczos Iteration\n\n The purpose of Lanczos iteration is to project a hermitian matrix A onto an orthonomalized Krylov subspace\n and thereby obtaining a tridiagonal matrix from which the highest eigenvalues can be estimated.\n Here the problem is further simplified by looking at a real symmetrical matrix A\n\n");
	
	if(n>N){
		printf("The chosen subspace dimension is larger than the dimensions of the Hermitian matrix A\n");
		return 1;
	}
	
	printf("The dimension of A is chosen to be N = %i and the dimension of the Krylov subspace is n = %i\n\n",N,n);
	
	gsl_matrix* A = gsl_matrix_alloc(N,N);
	gsl_matrix* Q = gsl_matrix_alloc(N,n);
	gsl_matrix* T = gsl_matrix_alloc(n,n);
	gsl_vector* q = gsl_vector_alloc(N);
	gsl_vector* q_temp = gsl_vector_alloc(N);
	gsl_vector* q_par = gsl_vector_alloc(N);
	gsl_vector* q_k = gsl_vector_alloc(N);
	gsl_vector* q_k_minus_one = gsl_vector_alloc(N);
	gsl_matrix* QTA = gsl_matrix_alloc(n,N);
	
	// Generate random hermitian matrix A
	for(int i=0;i<N;i++){ 	// Create random symmetrical matrix A			
		for(int j=i;j<N;j++){
			gsl_matrix_set(A,i,j,RND);
			gsl_matrix_set(A,j,i,gsl_matrix_get(A,i,j));
		}
	}
	
	printf("A real symmetrical matrix A is generated with random entries\n\n");
	printf("A = \n");
	matrix_print(A); // printf matrix A
	printf("\n");
	
	for(int i=0;i<N;i++){ // set random elements in q
		gsl_vector_set(q,i,RND);
	}
	
	double q_norm = gsl_blas_dnrm2(q);
	
	for(int i=0;i<N;i++){ // set random elements in q
		gsl_vector_set(q,i,gsl_vector_get(q,i)/q_norm);
	}
	printf("A vector q is generated with random entries. q is the first column in our matrix Q. We can get the remaining columns by setting q_k = A*q_(k-1) and making\n the new column orthonormal to all others.\n");
	printf("q = \n");
	vector_print(q);
	printf("\n");
	printf("norm of q = %g\n",gsl_blas_dnrm2(q));
	
	
	double q_dot;
	
	for(int i=0;i<N;i++){ // set first column in Q to be q
		gsl_matrix_set(Q,i,0,gsl_vector_get(q,i));
	}

	
	for(int k=1;k<n;k++){
		
		column_from_matrix(q_k_minus_one,Q,k-1);
		gsl_blas_dgemv(CblasNoTrans,1.0,A,q_k_minus_one,0.0,q_k);
		
		
		for(int i=0;i<k;i++){
			column_from_matrix(q,Q,i);	// sets q to be the i'th column vector of Q
			q_dot = dot(q_k,q);	// finds inner product betwee n q_k and q
			gsl_vector_memcpy(q_par,q); // makes q_parallel into a copy of q
			gsl_vector_scale(q_par,q_dot/gsl_blas_dnrm2(q_par)); // scales q_parallel to have the length of the inner product between q_k and q
			gsl_matrix_set(T,i,k-1,q_dot);
			gsl_vector_sub(q_k,q_par); // subtract the component of q_k parallel to q
			// printf("norm q_k = %g\n",gsl_blas_dnrm2(q_k));
		}
		
		gsl_matrix_set(T,k,k-1,gsl_blas_dnrm2(q_k));
		gsl_vector_scale(q_k,1/gsl_blas_dnrm2(q_k)); // normalize q_k
		
		for(int i=0;i<N;i++){ // set first column in Q to be q
		gsl_matrix_set(Q,i,k,gsl_vector_get(q_k,i));
	}
		
	}
	
		
	printf("\nQ = \n");
	matrix_print(Q);
	printf("\n");
	printf("Q is now a matrix with mutually orthonormal column vectors\n");
	printf("We can check that this is indeed the case\n\n");
	
	for(int i=0;i<n;i++){
	column_from_matrix(q,Q,i);
	printf("norm column %i = %g\n", i,gsl_blas_dnrm2(q));
	}
	printf("--> The column vectors are all normalized\n\n");
	for(int i=0;i<n;i++){
		for(int j=0;j<n;j++){
			if(i==j){
				break;
			}
			column_from_matrix(q,Q,i);
			column_from_matrix(q_par,Q,j);
			printf("dot(q_%i,q_%i) = %g\n",i,j,dot(q,q_par));
		}
	}
	printf("--> The column vectors are mutually orthonormal within the numerical error\n\n");
	
	
	gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0,Q,A,0.0,QTA);
	gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,QTA,Q,0.0,T);
	printf("T = Q^T*A*Q = \n");
	matrix_print(T);
	
	printf("\nThe matrix T is clearly tridiagonal and one can this fairly easily find the eigenvalues of such as matrix using QR decomposition\n This however, I have omitted here as it is not strictly part of this exam exercise.\n");
	
	
	gsl_matrix_free(A);
	gsl_matrix_free(Q);
	gsl_matrix_free(QTA);
	gsl_matrix_free(T);
	gsl_vector_free(q);
	gsl_vector_free(q_temp);
	gsl_vector_free(q_par);
	gsl_vector_free(q_k);
	gsl_vector_free(q_k_minus_one);
	
	return 0;
}