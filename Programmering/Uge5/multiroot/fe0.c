#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>
#include<math.h>

int ode_H0(double x, const double y[], double dydx[], void *params)
{
	double e=*(double*)params;
	dydx[0] = y[1];
	dydx[1] = -2*(1/x+e)*y[0];
	return GSL_SUCCESS;
}

double fe0(double e, double x)
{
	gsl_odeiv2_system sys;
	sys.function = ode_H0;
	sys.jacobian = NULL;
	sys.dimension = 2;
	sys.params = (void*)&e;
	double x_min = 0.0001;
	
	gsl_odeiv2_driver *driver;
	double hstart = 0.1, abs = 1e-12, eps = 1e-12;
	driver = gsl_odeiv2_driver_alloc_y_new(&sys,gsl_odeiv2_step_rkf45,hstart, abs, eps);
	double y[] = { x_min-pow(x_min,2), 1-2*x_min };
	gsl_odeiv2_driver_apply(driver, &x_min, x, y);
	gsl_odeiv2_driver_free(driver);
	return y[0];
}