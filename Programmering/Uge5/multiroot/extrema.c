#include<gsl/gsl_multiroots.h>
#include<gsl/gsl_errno.h>
#include<math.h>
#define TYPE gsl_multiroot_fsolver_hybrids
#define EPS 1e-12

int rosenbrock(const gsl_vector * x, void * params, gsl_vector * f)
{
	double x1 = gsl_vector_get(x,0);
	double x2 = gsl_vector_get(x,1);
	double mismatch_1 = 2*(200*pow(x1,3)-200*x1*x2+x1-1);
	double mismatch_2 = 200*(x2-pow(x1,2));
	gsl_vector_set(f,0,mismatch_1);
	gsl_vector_set(f,1,mismatch_2);
return GSL_SUCCESS;
}

gsl_vector *extrema(void){
	gsl_multiroot_function F;
	F.f = rosenbrock;
	F.n = 2;
	F.params = NULL;

	gsl_multiroot_fsolver * S;
	S = gsl_multiroot_fsolver_alloc(TYPE,F.n);

	gsl_vector* start = gsl_vector_alloc(F.n);
	gsl_vector* result = gsl_vector_alloc(F.n);
	gsl_vector_set(start, 0, 1.1);
	gsl_vector_set(start, 1, 0.9);
	gsl_multiroot_fsolver_set(S,&F,start);

	int flag,iter=0;
	do{
		iter++;
		gsl_multiroot_fsolver_iterate(S);
		flag = gsl_multiroot_test_residual(S->f,EPS);
	}while(flag==GSL_CONTINUE);
	fprintf(stderr,"iter=%i\n",iter);

	double result_x = gsl_vector_get(S->x,0); 
	double result_y = gsl_vector_get(S->x, 1);
	
	gsl_vector_set(result,0,result_x);
	gsl_vector_set(result,1,result_y);
	
	gsl_multiroot_fsolver_free(S);
	gsl_vector_free(start);
	return result;
	
}