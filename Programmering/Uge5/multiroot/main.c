#include<stdio.h>
#include<math.h>
#include<gsl/gsl_errno.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_multiroots.h>
#define TYPE gsl_multiroot_fsolver_hybrids
#define EPS 1e-6

gsl_vector *extrema(void);
double fe(double e,double xmax);
double fe0(double e,double x);

int root_equation (const gsl_vector * x, void * params, gsl_vector * f)
{
	double xmax = *(double*)params;
	double e = gsl_vector_get(x,0);
	double mismatch = fe(e,xmax);
	gsl_vector_set(f,0,mismatch);
	return GSL_SUCCESS;
}

double theoretical(double r){
	double y = r*exp(-r);
	return y;
}

int main(){
// 	First Excercise
	gsl_vector* extremum = extrema();
	
	FILE *mystream;
	mystream = fopen ("out.txt", "w+");
	fprintf(mystream, "f(x,y) = (%g,\t %g)\n",gsl_vector_get(extremum,0),gsl_vector_get(extremum,1));
	fclose(mystream);
	gsl_vector_free(extremum);
	
// 	Second Excercise
	double xmax = 8;
	double estart = -0.6;
	
	gsl_multiroot_function F;
	F.f = root_equation;
	F.n = 1;
	F.params=(void*)&xmax;

	gsl_multiroot_fsolver * S;
	S = gsl_multiroot_fsolver_alloc(TYPE,F.n);

	gsl_vector* start = gsl_vector_alloc(F.n);
	gsl_vector_set(start,0,estart);
	gsl_multiroot_fsolver_set(S,&F,start);

	int flag;
	do{
		gsl_multiroot_fsolver_iterate(S);
		flag=gsl_multiroot_test_residual(S->f,EPS);
	}while(flag==GSL_CONTINUE);

	double e0 = gsl_vector_get(S->x,0);
	gsl_multiroot_fsolver_free(S);
	gsl_vector_free(start);
	fprintf(stderr,"e = %g\n",e0);
	
	// Now that we know the energy we can determine the wave function for this particular energy
	FILE *mystream0;
	mystream0 = fopen ("wavefunction.data", "w+");
	for (double x = 0.001; x < xmax; x += 0.01){
		fprintf(mystream0,"%g %g %g\n",x,fe0(e0,x),theoretical(x));
	}
	fclose(mystream0);
return 0;	
}

