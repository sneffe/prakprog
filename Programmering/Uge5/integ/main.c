#include<stdio.h>
#include<math.h>
#include<qspline.h>

double func();
double numerator(double alpha);
double denominator(double alpha);

int main(){
double y = func();
FILE * fp;
fp = fopen ("out1.txt", "w+");
fprintf(fp, "integral = %g\n", y);
fclose(fp);

FILE * mystream;
mystream = fopen ("out2.txt", "w+");
	for(double alpha=0.1; alpha<5;alpha+=0.01){
	double y1 = numerator(alpha);
	double denom = denominator(alpha);
	double result = y1/denom;	
        fprintf(mystream, "%g %g\n",alpha, result);
}
fclose(mystream);

printf("We see that the minimum is at a value of alpha = 1; This, in fact, gives the correct value for the groundstate because we have chosen a trial wavefunction that has the correct form for a harmonic oscillator.\n");

return 0;
}
