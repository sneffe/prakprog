#include<math.h>
#include<gsl/gsl_integration.h>
#include<gsl/gsl_errno.h>

struct par {double alpha;};

double integranddd (double x, void * params) {
	struct par p = *(struct par *) params;
	double alpha = p.alpha;
	double f = exp(-alpha*x*x);
	return f;
}

double denominator(double alpha){
	int limit=100;
	gsl_integration_workspace * w;
	w = gsl_integration_workspace_alloc (limit);

	struct par params = {.alpha=alpha};
	gsl_function F;
	F.function = integranddd;
	F.params = (void*)&params;

	double result,error,acc=1e-8,eps=1e-8;
	int flag = gsl_integration_qagi
		(&F, acc, eps, limit, w, &result, &error);

	gsl_integration_workspace_free(w);

	if(flag!=GSL_SUCCESS) return NAN;
	return result;
}
