#include "nvector.h"
#include "stdio.h"
#include "stdlib.h"
#define RND (double) rand()/RAND_MAX // Random double number between 0 and 1
int main(){
	int n =5;
	printf("\nmain: testing nvector_alloc ... \n");
	nvector *v = nvector_alloc(n);
	if(v == NULL){ printf("Test Failed\n");}else {printf("Test Passed\n");}
	printf("\nmain: testing nvector_set and nvector_set ... \n"); // We define one entrance in the vector. We then asks nvector what that is and compare it to the assigned value
	double value = RND;
	int i = n / 2;
	nvector_set(v,i,value);
	double vi = nvector_get(v,i);
	if (vi==value){printf("Test Passed\n");} else{printf("Test Failed\n");}

	printf("\nmain: testing nvector_add ... \n");
	nvector *a = nvector_alloc(n); // We allocate memory for 3 different vectors a,b, and c
	nvector *b = nvector_alloc(n);
	nvector *c = nvector_alloc(n); //We define the entries in the vectors through a for loop that is restricted by the size of the vectors n
	nvector *d = nvector_alloc(n);
	nvector *j = nvector_alloc(n);
	for (int i = 0; i < n; i++) {
		double x = RND;
		double y = RND;
		nvector_set(a,i,x);
		nvector_set(b,i,y);
		nvector_set(c,i,x + y);
		nvector_set(j,i,x-y);
	}
	d = nvector_add(a, b);
	nvector_print("a = ", a);
	nvector_print("b = ", b);
	nvector_print("a+b should = ",c);
	nvector_print("a+b actually = ", d);

	printf("\nmain: testing nvector_sub ... \n");
	d = nvector_sub(a, b);
        nvector_print("a-b should = ",j);
        nvector_print("a-b actually = ", d);

	printf("\nmain: testing nvector_scale ... \n");
        double k = 2;
	d = nvector_scale(a, k);
        nvector_print("2*a is = ",d);
	
	nvector_free(v); // free allocated memory
	nvector_free(a);
	nvector_free(b);
	nvector_free(c);
	return 0;
}
