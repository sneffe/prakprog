#include<stdio.h>
#include"nvector.h"
#include<stdlib.h>
nvector* nvector_alloc(int n){
  nvector* v = malloc(sizeof(nvector));
  (*v).size = n;
  (*v).data = malloc(n*sizeof(double));
  if( v==NULL ) fprintf(stderr,"error in nvector_alloc\n");
  return v;
}

void nvector_free(nvector* v) {
	free((*v).data);
	free(v);
} /* v->data is identical to (*v).data */

void nvector_set(nvector* v, int i, double value) {
	(*v).data[i]=value;
}

double nvector_get(nvector* v, int i) {
	return (*v).data[i];
}

void nvector_print(char *c,nvector *v){
	int n  = (*v).size;
	printf("%s", c);
	printf("[");
	for(int i = 0; i<n;i++){
		printf("%3.3f", nvector_get(v,i));
		if(i != n-1) printf(",\t");
	}
	printf("]\n");
}

nvector* nvector_add(nvector *a, nvector *b) {
	int n  = (*a).size;
	int m  = (*b).size;
	if(m!=n) {
		fprintf(stderr,"The two vectors must have the same length\n");
	}
	nvector *c = nvector_alloc(n);
	for(int i=0;i < n;i++){
		double ci = nvector_get(a,i) + nvector_get(b,i);
		nvector_set(c,i,ci);
	}
	return c;
}

nvector* nvector_sub(nvector *a, nvector *b) {
        int n  = (*a).size;
        int m  = (*b).size;
        if(m!=n) {
                fprintf(stderr,"The two vectors must have the same length\n");
        }
        nvector *c = nvector_alloc(n);
        for(int i=0;i < n;i++){
                double ci = nvector_get(a,i) - nvector_get(b,i);
                nvector_set(c,i,ci);
        }
        return c;
}

nvector* nvector_scale(nvector *a, double x) {
        int n  = (*a).size;
        nvector *c = nvector_alloc(n);
        for(int i=0;i < n;i++){
                double ci = nvector_get(a,i) * x;
                nvector_set(c,i,ci);
        }
        return c;
}
