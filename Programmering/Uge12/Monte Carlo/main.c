#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<math.h>

void plainmc(int dim, gsl_vector* a, gsl_vector* b, double f(gsl_vector* x), int N, double* result, double* error);
double f(gsl_vector* x);

int main(){
	
	int dim = 2;
	int N = 1e5;
	double result;
	double error;
	
	gsl_vector* a = gsl_vector_alloc(dim);
	gsl_vector* b = gsl_vector_alloc(dim);
	
	gsl_vector_set(a,0,0);
	gsl_vector_set(a,1,0);
	gsl_vector_set(b,0,M_PI);
	gsl_vector_set(b,1,M_PI);
	
	plainmc(dim, a, b, f, N, &result, &error);
	
	printf("--- Monte Carlo Integration ---\n\n");
	printf("Exercise A\n");
	printf("function to integrate: f(x,y) = exp(-(x^2+y^2))\n");
	printf("Number of random points: %i\n",N);
	printf("a = {%g, %g}\n",gsl_vector_get(a,0),gsl_vector_get(a,1));
	printf("b = {%g, %g}\n",gsl_vector_get(b,0),gsl_vector_get(b,1));
	printf("result = %g\n", result);
	printf("error = %g\n", error);
	printf("expected result = 1/4*erf(pi)^2 = 0.785384\n");
	
	FILE * datastream; //Open a data file and print the data points into it
	datastream = fopen("data.txt", "w+");
	for(int i=1000; i<N; i+=1000){
		plainmc(dim, a, b, f, i, &result, &error);
		fprintf(datastream,"%i\t%g\n",i,error);
	}
	fclose(datastream);
	printf("\n");
		
	printf("Exercise B\n");
	printf("I would like to show that the error estimate of the Monte Carlo integration behaves as 1/sqrt(N), where N is the number of points.\n From plot.svg it is obvious that this is indeed the case, which is also supported by the fit using Gnuplot.\n");

	gsl_vector_free(a);
	gsl_vector_free(b);
	
	return 0;
}

double f(gsl_vector* x){
	return exp(-(pow(gsl_vector_get(x,0),2)+pow(gsl_vector_get(x,1),2))); // constant function for now
}