#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<math.h>
#define RND ((double) rand ( ) /RAND_MAX)

void randomx(int dim, gsl_vector* a, gsl_vector* b, gsl_vector* x){
	for(int i = 0; i < dim; i++){
		gsl_vector_set(x,i,RND*(gsl_vector_get(b,i)-gsl_vector_get(a,i)));
	}
}

void plainmc(int dim, gsl_vector* a, gsl_vector* b, double f(gsl_vector* x), int N, double* result, double* error){
		
		gsl_vector* x = gsl_vector_alloc(dim);
		
		double V = 1;
		
		for(int i = 0; i < dim; i++){
			V *= gsl_vector_get(b,i)-gsl_vector_get(a,i);
		}
		
		double sum = 0;
		double sum2 = 0;
		double fx;
		
		for(int i = 0; i < N; i++){
			randomx(dim,a,b,x);
			fx = f(x);
			sum += fx;
			sum2 += fx*fx;
		}
		
		double avr = sum/N;
		double var = sum2/N-avr*avr;
		
		*result = avr*V;
		*error = sqrt(var/N)*V;
		
		gsl_vector_free(x);
}