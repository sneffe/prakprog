#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<math.h>
#include"ann.h"

double activation_function(double x){return x*exp(-x*x);}
double activation_function_derivative(double x){return exp(-x*x)-2*x*x*exp(-x*x);}
double activation_function_primitive(double x){return -exp(-x*x)/2;}
double function_to_fit(double x){return sin(x);}

int main(){
	int n = 10; // number of neurons
	ann* network = ann_alloc(n,activation_function,activation_function_derivative,activation_function_primitive); // allocate articifical neural network
	
	double a = -3; //start of interval of points
	double b = 3; // end of interval of points
	
	int nx = 20; // number of data points to be interpolated
	
	gsl_vector* vx = gsl_vector_alloc(nx); // x-vector
	gsl_vector* vy = gsl_vector_alloc(nx); // y-vector
	
	for(int i=0;i<nx;i++){
		double x=a+(b-a)*i/(nx-1);
		double f=function_to_fit(x);
		gsl_vector_set(vx,i,x); // vx contains a vector of x'es
		gsl_vector_set(vy,i,f); // vy contains the function values
	}
	
	FILE * stream2; //Open a data file and print the data points into it
	stream2 = fopen("data.txt", "w+");
	for(int i=0;i<nx;i++){
		fprintf(stream2,"%g\t %g\n",gsl_vector_get(vx,i),gsl_vector_get(vy,i));
	}
	fclose(stream2);
	
	for(int i=0;i<network->n;i++){
		gsl_vector_set(network->data, 3*i+0, a+(b-a)*i/(network->n-1));
		gsl_vector_set(network->data, 3*i+1,1);
		gsl_vector_set(network->data, 3*i+2,1);	
	}
	
	ann_train(network,vx,vy);
	
	FILE * stream1; //Open a data file and print the data points into it
	stream1 = fopen("intp.txt", "w+");
	double dz=1.0/128;
	for(double z=a;z<=b;z+=dz){  //make list of interpolated values in steps dz
		double y=ann_feed_forward(network,z);
		double dy = ann_feed_forward2(network,z);
		double Y = ann_feed_forward3(network,z)-ann_feed_forward3(network,0)-1;
		fprintf(stream1,"%g\t %g\t %g\t %g\n",z,y,dy,Y);
	}
	fclose(stream1);
	
	gsl_vector_free(vx);
	gsl_vector_free(vy);
	ann_free(network);
	return 0;
}

