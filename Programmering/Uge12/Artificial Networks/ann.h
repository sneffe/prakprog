#include<gsl/gsl_vector.h>
#ifndef HAVE_NEURONS
#define HAVE_NEURONS

typedef struct{
	int n;
	double(*f)(double);
	double(*g)(double);
	double(*h)(double);
	gsl_vector* data;
	} ann;

ann* ann_alloc(int n,double(*f)(double),double(*g)(double),double(*h)(double));
void ann_free(ann* network);
double ann_feed_forward(ann* network,double x);
double ann_feed_forward2(ann* network,double x);
double ann_feed_forward3(ann* network,double x);
void ann_train(ann* network,gsl_vector* xlist,gsl_vector* ylist);
#endif