#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<pthread.h>

int N = 1e7; // Number of points

void* rand_vector(void* arg){
	gsl_vector* x = (gsl_vector*)arg;
	
	unsigned int seed;	
	double random;
	for(int i = 0;i<N;i++){
		random = (double)rand_r(&seed)/((double)RAND_MAX);	
		gsl_vector_set(x,i,random);
	}
	return NULL;
}

int main(){
	
	int N_in = 0; // Number of points inside the circle
	double r = 1; // radius of the circle
	
	pthread_t thread;
	
	gsl_vector* x = gsl_vector_alloc(N);
	gsl_vector* y = gsl_vector_alloc(N);
	
	int flag = pthread_create(&thread, NULL, rand_vector, (void*)x);
	rand_vector((void*)y);
	
	flag = pthread_join(thread,NULL);
	
	  	for(int i = 0; i<N; i++){
		if(sqrt(gsl_vector_get(x,i)*gsl_vector_get(x,i)+gsl_vector_get(y,i)*gsl_vector_get(y,i))<= r){
			N_in += 1;
		}
	}
	printf("--- Multi-threading ---\n\n");
	printf("Using pthread\n");
	printf("Number of points: N = %i\n",N);
	printf("Number of points inside: N = %i\n",N_in);
	printf("We can estimate pi as: pi = 4*N_in/N\n");
	printf("pthread threading estimate of pi is: %g\n",4*(double)N_in/(double)N);
	
	
	gsl_vector_free(x);
	gsl_vector_free(y);
	
	return 0;
}