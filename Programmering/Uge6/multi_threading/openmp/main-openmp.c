#include<stdio.h>
#include<stdlib.h>
#include<omp.h>
#include<math.h>
#include<gsl/gsl_vector.h>

int main(){
	
	int N = 1e7;
	int N_in = 0; // Number of points inside the circle
	double r = 1; // radius of the circle
	
	gsl_vector* x = gsl_vector_alloc(N);
	gsl_vector* y = gsl_vector_alloc(N);
	
	#pragma omp parallel sections
	// the following sections wil be run parallelly in separate threads
	{
		#pragma omp section  // first thread will run this block of code
		{		
			unsigned int seed1;	
			double random1;
			for(int i = 0;i<N;i++){
			random1 = (double)rand_r(&seed1)/((double)RAND_MAX);	
			gsl_vector_set(x,i,random1);
			}
		}  
		
		#pragma omp section  // second thread will run this block of code
		{  	
			unsigned int seed2;	
			double random2;
			for(int i = 0;i<N;i++){
			random2 = (double)rand_r(&seed2)/((double)RAND_MAX);	
			gsl_vector_set(y,i,random2);
			}
		}
	}
	
  	for(int i = 0; i<N; i++){
		if(sqrt(gsl_vector_get(x,i)*gsl_vector_get(x,i)+gsl_vector_get(y,i)*gsl_vector_get(y,i))<= r){
			N_in += 1;
		}
	}
	printf("--- Multi-threading ---\n\n");
	printf("Using openMP\n");
	printf("Number of points: N = %i\n",N);
	printf("Number of points inside: N = %i\n",N_in);
	printf("We can estimate pi as: pi = 4*N_in/N\n");
	printf("openmp threading estimate of pi is: %g\n",4*(double)N_in/(double)N);
	
	gsl_vector_free(x);
	gsl_vector_free(y);
}