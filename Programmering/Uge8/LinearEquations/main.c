#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include"qr.h"
#define RND ((double)rand()/RAND_MAX)  // Needed to get random matrix elements
#define FMT "%7.3f"


void matrix_print(gsl_matrix* A){   // prints elements of gsl matrix of size (size1, size2)
	for(int r=0;r<A->size1;r++){
		for(int c=0;c<A->size2;c++) printf(FMT,gsl_matrix_get(A,r,c));
		printf("\n");}
}

void vector_print(gsl_vector* v){   // prints elements of gsl matrix of size (size1, size2)
	for(int r=0;r<v->size;r++){
		printf("%g\n",gsl_vector_get(v,r));}
}

int main(){
	printf("---Exercise A Part I---\n");
	size_t n = 5, m=3;  //Matrix Dimensions. Here n must be larger than or equal to m.
	gsl_matrix* A = gsl_matrix_calloc(n,m); // Allocate space for Matrix A to be QR decomposed and Triangular matrix R. All elements are initially set to 0.
	gsl_matrix* R = gsl_matrix_calloc(m,m);
	
	for(int i=0;i<n;i++){ 				// Fill in random elements in A
		for(int j=0;j<m;j++){
			gsl_matrix_set(A,i,j,RND); 
		}
	}
	//matrix_print(A);
	printf("A =\n");
	matrix_print(A);
	printf("\n");
	printf("Q = \n");
	qrdec(A,R);
	matrix_print(A);
	printf("\n R = \n");
	matrix_print(R);
	printf("\n");
	
	gsl_matrix* qtq = gsl_matrix_calloc(m,m);
	gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0,A,A,0.0,qtq);
	printf("Q^T*Q = \n");
	matrix_print(qtq);
	printf("\n");
	
	gsl_matrix* qr = gsl_matrix_calloc(n,m);
	gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,A,R,0.0,qr);
	
	printf("Q*R = \n");
	matrix_print(qr);
	printf("\n");
	
	printf("---Exercise A Part II---\n");
	m = 3;
	gsl_matrix* AA = gsl_matrix_calloc(m,m); // New square matrix A
	gsl_matrix* RR = gsl_matrix_calloc(m,m); // New Square upper triangular matrix RR 
	
	for(int i=0;i<m;i++){ 				// Fill in random elements in AA
		for(int j=0;j<m;j++){
			gsl_matrix_set(AA,i,j,RND); 
		}
	}

	gsl_vector* x = gsl_vector_alloc(m); //Prepare vector x with solution to the linear equations
	gsl_vector* b = gsl_vector_alloc(m); //Prepare vector b with right hand side of the linear equations
		
	for(int i=0;i<m;i++){ 				// Fill in right hand side of linear equations
			gsl_vector_set(b,i,RND); 
	}
	
	printf("AA = \n"); // Print out AA
	matrix_print(AA);
	printf("\n");
	
	qrdec(AA,RR);	//QR decompose AA
	qrsolve(AA,RR,b,x); //Solve the linear equation and obtain solution x
	
	printf("RR = \n");
	matrix_print(RR); 
	printf("\n");
	
	printf("QQ = \n");
	matrix_print(AA);
	printf("\n");
	
	printf("x = \n");
	vector_print(x);
	printf("\n");
	
	printf("b = \n");
	vector_print(b);
	printf("\n");
	
	gsl_vector* b_check = gsl_vector_alloc(m);
	gsl_matrix* qqrr = gsl_matrix_alloc(m,m);
	gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,AA,RR,0.0,qqrr);
	
	gsl_blas_dgemv(CblasNoTrans, 1.0,qqrr, x, 0.0, b_check);
	
	printf("b_check = \n");
	vector_print(b_check);
	printf("\n");
	
	printf("NICE!\n");
	
	printf("---Exercise B---\n");
	
	m = 4;
	gsl_matrix* AAA = gsl_matrix_calloc(m,m); // New square matrix AAA
	gsl_matrix* RRR = gsl_matrix_calloc(m,m); // New Square upper triangular matrix RRR 
	gsl_matrix* B = gsl_matrix_calloc(m,m); 
	
	for(int i=0;i<m;i++){ 				// Fill in random elements in AAA
		for(int j=0;j<m;j++){
			gsl_matrix_set(AAA,i,j,RND); 
		}
	}
	printf("AAA = \n");
	matrix_print(AAA);
	printf("\n");
	
	qrdec(AAA,RRR);

	printf("QQQ = \n");
	matrix_print(AAA);
	printf("\n");
	
	printf("RRR = \n");
	matrix_print(RRR);
	printf("\n");
	
	qrinv(AAA, RRR, B);
	
	printf("B = \n");
	matrix_print(B);
	printf("\n");
	
	gsl_matrix* A_original = gsl_matrix_calloc(m,m);
	gsl_matrix* Identity = gsl_matrix_calloc(m,m);
	gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,AAA,RRR,0.0,A_original);
	

	gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,A_original,B,0.0,Identity);
	
	printf("A*A^-1 = \n");
	matrix_print(Identity);
	printf("\n");
	printf("Nice again!\n");
	
	
	gsl_matrix_free(A);
	gsl_matrix_free(R);
	gsl_matrix_free(qtq);
	gsl_matrix_free(qr);
	gsl_vector_free(x);
	gsl_vector_free(b);
	gsl_vector_free(b_check);
	gsl_matrix_free(AA);
	gsl_matrix_free(RR);
	gsl_matrix_free(qqrr);
	gsl_matrix_free(AAA);
	gsl_matrix_free(RRR);
	gsl_matrix_free(B);
	gsl_matrix_free(A_original);
	gsl_matrix_free(Identity);
	return 0;
}