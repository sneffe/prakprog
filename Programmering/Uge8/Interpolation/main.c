#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include"qspline.h"

double linterp(int n ,double* x , double* y , double z );
double linterp_integ(int n, double *x, double *y, double z);
double qspline_integ(int n,double* x,double* y,double z);
double qspline_derivative(int n,double* x,double* y,double z);

int main(){
	int n = 20;

	double x[n];
	double y[n];
	FILE * fp;
	fp = fopen ("data.txt", "w+");
	
	for(int i = 0; i <= n-1; i++)
	{
		x[i] = i;
		y[i] = sin(i);
		fprintf(fp,"%g %g\n",x[i],y[i]);
	}
	fclose(fp);
	
	qspline* s = qspline_alloc(n, x, y);
	
	for(double j = 0.001;j < x[n-1]; j+=0.1){
		printf("%g %g %g %g %g %g\n", j, linterp(n ,x ,y, j), linterp_integ(n,x,y,j), qspline_eval(s,j), qspline_integ(n,x,y,j),qspline_derivative(n,x,y,j));	
	}
	qspline_free(s);
return 0;
}