#include<assert.h>

double linterp (int n ,double* x , double* y , double z ){
	assert(n>1 && z>=x[0] && z<=x[n-1]); //ensure that z is within the list boundaries
	int i = 0, j=n-1; //binary search //
	while(j-i >1){ int m =(i+j)/2; if (z>x[m]) i=m; else j=m;}
	return y[i]+(y[i+1]-y[i])/(x[i+1]-x[i]) * (z-x[i]);
}

double linterp_integ(int n, double *x, double *y, double z){
	double integ = 0;
	int i = 0, j=n-1; //binary search //
	double yz = linterp(n,x,y,z);
	while(j-i >1){ int m =(i+j)/2; if (z>x[m]) i=m; else j=m;}
	integ += (z-x[i])*y[i] + 0.5*(z-x[i])*(yz-y[i]);	//get the last bit of area
	
	int k = 0;
	while(k <= i-1){
		integ += (x[k+1]-x[k])*y[k] + 0.5*(x[k+1]-x[k])*(y[k+1]-y[k]);  //Add up area of 
		k++;
	}
	return integ;
}