#include<gsl/gsl_sf.h>
#include"make_airy_plot.h"
#include<math.h>
void make_airy_plot(void){
	double lower_bnd = -15.0;
	double upper_bnd = 5.0;

	for(double x=lower_bnd; x < upper_bnd;x+=0.05){
		double A = gsl_sf_airy_Ai(x,GSL_PREC_DOUBLE);
		double B = gsl_sf_airy_Bi(x,GSL_PREC_DOUBLE);
		printf("%g\t%g\t%g\n",x,A,B);
	}
}
