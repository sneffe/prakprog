#include<stdio.h>
#include<stdlib.h>
int  equal(double a, double b, double tau, double epsilon)
{
        int returnValue;
        if(abs(a-b)<tau){returnValue = 1;}
        else if(abs(a-b)/(abs(a)+abs(b))<(epsilon/2)){returnValue = 1;}
        else{returnValue = 0;}
        return returnValue;
}
