#include<limits.h>
#include<float.h>
#include<stdio.h>
#include"equal.h" //my own header file
int main(){
	int i = 1;
	printf("---Exercise 1) i: Largest integer number---\n");
	printf("INT MAX = %i\n",INT_MAX);
	
	while(i+1 > i){i++;}
	printf("while max int = %i\n",i);
	
	for(i = 1; i<i+1; i++){}
	printf("for max int = %i\n",i);
	
	i=1;
	do {i++;} while(i<i+1);
	printf("do while max int = %i\n\n",i);

	printf("---Exercise 1) ii: Lowest integer number---\n");
	printf("INT MIN = %i\n",INT_MIN);
	
	i=1;
	while(i-1 < i){i--;}
        printf("while min int = %i\n",i);

	for(i = 1; i>i-1; i--){}
        printf("for min int = %i\n",i);
	
	i=1;
        do {i--;} while(i>i-1);
        printf("do while min int = %i\n\n",i);

	printf("---Exercise 1) iii: Machine Epsilon---\n");
	printf("FLT_EPSILON = %g\n",FLT_EPSILON);
	printf("DBL_EPSILON = %g\n",DBL_EPSILON);
	printf("LDBL_EPSILON = %Lg\n",LDBL_EPSILON);
	
	float x=1; while(1+x!=1){x/=2;} x*=2; printf("(While) My Epsilon Float = %g\n",x);
	double y=1; while(1+y!=1){y/=2;} y*=2; printf("(While) My Epsilon Double = %g\n",y);
	long double z=1; while(1+z!=1){z/=2;} z*=2; printf("(While) My Epsilon Long Double = %Lg\n",z);

 	x=1;do{x/=2;} while(1+x!=1); x*=2;printf("(Do While) My Epsilon Float = %g\n",x);
        y=1;do{y/=2;} while(1+y!=1); y*=2;printf("(Do While) My Epsilon Double = %g\n",y);
	z=1;do{z/=2;} while(1+z!=1); z*=2;printf("(Do While) My Epsilon Long Double = %Lg\n",z);

	for(x=1;1+x!=1;x/=2){;} x*=2;printf("(For) My Epsilon Float = %g\n",x);
       	for(y=1;1+y!=1;y/=2){;} y*=2;printf("(For) My Epsilon Double = %g\n",y);
        for(z=1;1+z!=1;z/=2){;} z*=2;printf("(For) My Epsilon Long Double = %Lg\n",z);
	printf("Very Nice!\n\n");

	printf("---Exercise 2) i---\n");
	int max=INT_MAX/2;
	float sum_up_float = 1;
	float sum_down_float = 0;
	for(i=2;i<=max;i++){sum_up_float = sum_up_float + 1.0f/i;}  printf("sum up float %g\n",sum_up_float);
	for(i=max;i>=1;i--){sum_down_float = sum_down_float + 1.0f/i;} printf("sum down float %g\n",sum_down_float);
	
	printf("---Exercise 2) ii---\n");
	printf("The difference is in the order in which the approximations take place\n");

	printf("---Exercise 2) iii---\n");
	printf("No! This is but a partial sum of the harmonic series which is divergent.\n");

	printf("---Exercise 2) iV---\n");
	double sum_up_double = 1;
        double sum_down_double = 0;
        for(i=2;i<=max;i++){sum_up_double = sum_up_double + 1.0f/i;}  printf("sum up double %g\n",sum_up_double);
        for(i=max;i>=1;i--){sum_down_double = sum_down_double + 1.0f/i;} printf("sum down double %g\n",sum_down_double);
	printf("The error from the rounding of numbers is now insignificant because our partial sums can hold more digits.\n\n");

	printf("---Exercise 3 i)---\n");
	y = 1;
	double j = 0;
	printf("equal(1,1,1,1) = %i\n",equal(y,y,y,y)); 
	printf("equal(2,1,0,1) = %i\n",equal(2*y,y,j,y));
	printf("equal(1,0,1,0) = %i\n",equal(y,j,y,j));

	return 0;
}
