#include"komplex.h"
#include"stdio.h"
#define TINY 1e-6

int main(){
	komplex a = {4,7}, b = {6,9};

	printf("testing komplex_add...\n");
	komplex r = komplex_add(a,b);
	komplex R = {10,16};
	komplex_print("a=",a);
	komplex_print("b=",b);
	komplex_print("a+b should   = ", R);
	komplex_print("a+b actually = ", r);
}
