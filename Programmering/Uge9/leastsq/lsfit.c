#include<assert.h>
#include<gsl/gsl_blas.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include"qr.h"
#include<math.h> /* NAN is here */
#define FMT "%7.3f"


void backsub(gsl_matrix *R, gsl_vector *c);

void lsfit(int m, double funs(int i,double x),gsl_vector* x, gsl_vector* y, gsl_vector* dy, gsl_vector* c, gsl_matrix* S){
	int rows = x->size;
	gsl_matrix* A = gsl_matrix_calloc(rows,m);
	gsl_matrix* R = gsl_matrix_calloc(m,m);
	gsl_matrix* R_inv = gsl_matrix_calloc(m,m);
	gsl_vector* b = gsl_vector_alloc(rows);
	gsl_matrix* I = gsl_matrix_calloc(m,m);
	gsl_matrix_set_identity(I);
	
	for(int i = 0; i<rows;i++){
		for(int j = 0; j<m; j++){
			gsl_matrix_set(A,i,j,funs(j, gsl_vector_get(x,i))/gsl_vector_get(dy,i));
		}
	}
	
	for(int i=0;i<rows;i++){
		gsl_vector_set(b,i,gsl_vector_get(y,i)/gsl_vector_get(dy,i));
	}
	
	qrdec(A,R); // A is now replaced with Q in the QR-decomposition of A
	gsl_blas_dgemv(CblasTrans, 1.0, A, b, 0.0, c); // calculate the right hand side vector of the linear equation R*c = Q^T * b = p
	qrinv(I,R,R_inv);
	
	
	gsl_blas_dgemm(CblasNoTrans,CblasTrans,1.0,R_inv,R_inv,0.0,S);
	backsub(R,c); // We solve the linear equation now through backsubstituation since R is upper triangular
	
	gsl_matrix_free(A);
	gsl_matrix_free(R);
	gsl_matrix_free(R_inv);
	gsl_vector_free(b);
}

void lsfit1(int m, double funs1(int i,double x),gsl_vector* x, gsl_vector* y, gsl_vector* dy, gsl_vector* c, gsl_matrix* S){
	int rows = x->size;
	gsl_matrix* A = gsl_matrix_calloc(rows,m);
	gsl_matrix* R = gsl_matrix_calloc(m,m);
	gsl_matrix* R_inv = gsl_matrix_calloc(m,m);
	gsl_vector* b = gsl_vector_alloc(rows);
	gsl_matrix* I = gsl_matrix_calloc(m,m);
	gsl_matrix_set_identity(I);
	
	for(int i = 0; i<rows;i++){
		for(int j = 0; j<m; j++){
			gsl_matrix_set(A,i,j,funs1(j, gsl_vector_get(x,i))/gsl_vector_get(dy,i));
		}
	}
	
	for(int i=0;i<rows;i++){
		gsl_vector_set(b,i,gsl_vector_get(y,i)/gsl_vector_get(dy,i));
	}
	
	qrdec(A,R); // A is now replaced with Q in the QR-decomposition of A
	gsl_blas_dgemv(CblasTrans, 1.0, A, b, 0.0, c); // calculate the right hand side vector of the linear equation R*c = Q^T * b = p
	qrinv(I,R,R_inv);
	
	
	gsl_blas_dgemm(CblasNoTrans,CblasTrans,1.0,R_inv,R_inv,0.0,S);
	backsub(R,c); // We solve the linear equation now through backsubstituation since R is upper triangular
	
	gsl_matrix_free(A);
	gsl_matrix_free(R);
	gsl_matrix_free(R_inv);
	gsl_vector_free(b);
}


void backsub(gsl_matrix *R, gsl_vector *c){
int m=R->size1;
for(int i=m-1;i>=0;i--){
	double s=0;
	for(int k=i+1;k<m;k++) s+=gsl_matrix_get(R,i,k)*gsl_vector_get(c,k);
	gsl_vector_set(c,i,(gsl_vector_get(c,i)-s)/gsl_matrix_get(R,i,i));
	}
}

