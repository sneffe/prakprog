#include<stdio.h>
#include<gsl/gsl_blas.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<math.h>
#include"fit.h"
#define FMT "%7.3f"

void matrix_print(gsl_matrix* A);
double funs(int i, double x);
double funs1(int i, double x);

int main(){
	
	printf("The data that is to be fitted is:\n");
	int m = 3; // number of functions used for fit 
	gsl_vector* c = gsl_vector_alloc(m); // vector prepared for the final function coefficients achieved from the fit
	gsl_matrix* S = gsl_matrix_calloc(m,m); // Covariance Matrix
	gsl_matrix* S1 = gsl_matrix_calloc(m,m); // Covariance Matrix
	gsl_vector* c1 = gsl_vector_alloc(m);
	
	double x[] = {0.1, 1.33, 2.55, 3.78, 5, 6.22, 7.45, 8.68, 9.9}; //Data
	double y[] = {-15.5, 0.32, 2.45, 2.75, 2.27, 1.35, 0.157, -1.23, -2.75};
	double dy[] = {1.04, 0.594, 0.983, 0.983, 0.998, 1.11, 0.398, 0.535, 0.968, 0.478};
	int n = sizeof(x)/sizeof(x[0]); //number of data points
	
	printf("x\t y\t dy\n");
	for(int i=0;i<n;i++){
		printf("%g\t %g\t %g\n",x[i],y[i],dy[i]);
	}
	printf("\n\n");
	
	gsl_vector* vx = gsl_vector_alloc(n);
	gsl_vector* vy = gsl_vector_alloc(n);
	gsl_vector* vdy = gsl_vector_alloc(n);
	gsl_vector* c_sigma = gsl_vector_alloc(m);
	gsl_vector* c_sigma1 = gsl_vector_alloc(m);
	
	for(int i=0;i<n;i++){  //make lists into gsl vectors 
		gsl_vector_set(vx,i,x[i]);
		gsl_vector_set(vy,i,y[i]);
		gsl_vector_set(vdy,i,dy[i]);
		}
	
	lsfit(m, funs, vx, vy, vdy, c, S);
	
	for(int i=0;i<m;i++){
		gsl_vector_set(c_sigma, i,sqrt(gsl_matrix_get(S,i,i)));
	}
	
	FILE * datastream; //Open a data file and print the data points along with the fitted point into it
	datastream = fopen("data.txt", "w+");
	
	for(int i=0; i<n; i++){
	fprintf(datastream,"%g\t %g\t %g\n", gsl_vector_get(vx,i),gsl_vector_get(vy,i),gsl_vector_get(vdy,i));
	}
	fclose(datastream);
	
	FILE * fitstream;
	fitstream = fopen("fit.txt","w+");
	for(double i=0;i<10;i+=0.1){
		fprintf(fitstream, "%g %g %g %g\n",i,gsl_vector_get(c,0)*funs(0,i) + gsl_vector_get(c,1)*funs(1,i) + gsl_vector_get(c,2)*funs(2,i), (gsl_vector_get(c,0)+gsl_vector_get(c_sigma,0))*funs(0,i) + (gsl_vector_get(c,1)+gsl_vector_get(c_sigma,1))*funs(1,i) + (gsl_vector_get(c,2)+gsl_vector_get(c_sigma,2))*funs(2,i), (gsl_vector_get(c,0)-gsl_vector_get(c_sigma,0))*funs(0,i) + (gsl_vector_get(c,1)-gsl_vector_get(c_sigma,1))*funs(1,i) + (gsl_vector_get(c,2)-gsl_vector_get(c_sigma,2))*funs(2,i));
	}
	fclose(fitstream);
	
	printf("Basis of functions: 1, x, log(x)\n\n");
	
	printf("Fitted coefficients for the chosen basis of functions c = \n");
	for(int i=0;i<m;i++){
		printf("%g\n",gsl_vector_get(c,i));
	}
	printf("\n");
	
	
	printf("Covariance Matrix S = \n");
	matrix_print(S);
	printf("\n");
	
	// Now for a different set of basis functions
	lsfit1(m, funs1, vx, vy, vdy, c1, S1);
	for(int i=0;i<m;i++){
		gsl_vector_set(c_sigma1, i,sqrt(gsl_matrix_get(S1,i,i)));
	}
	
	FILE * fitstream1;
	fitstream1 = fopen("fit1.txt","w+");
	for(double i=0;i<10;i+=0.1){
		fprintf(fitstream1, "%g %g %g %g\n",i,gsl_vector_get(c1,0)*funs1(0,i) + gsl_vector_get(c1,1)*funs1(1,i) + gsl_vector_get(c1,2)*funs1(2,i), (gsl_vector_get(c1,0)+gsl_vector_get(c_sigma1,0))*funs1(0,i) + (gsl_vector_get(c1,1)+gsl_vector_get(c_sigma1,1))*funs1(1,i) + (gsl_vector_get(c1,2)+gsl_vector_get(c_sigma1,2))*funs1(2,i), (gsl_vector_get(c1,0)-gsl_vector_get(c_sigma1,0))*funs1(0,i) + (gsl_vector_get(c1,1)-gsl_vector_get(c_sigma1,1))*funs1(1,i) + (gsl_vector_get(c1,2)-gsl_vector_get(c_sigma1,2))*funs1(2,i));
	}
	fclose(fitstream1);
	
	printf("I now move on to fitting the data using a different set of basis functions\n\n");
	printf("Basis of functions: 1, x, x^2\n\n");
	
	printf("Here I get\n\n");
	printf("c1 =\n");
	for(int i=0;i<m;i++){
		printf("%g\n",gsl_vector_get(c1,i));
	}
	printf("\n\n");
	printf("S1 =\n");
	matrix_print(S1);
	printf("\n\n");
	
	printf("Both of these fits can be found as plot.svg and plot1.svg respectively.\n");
	
	
	gsl_vector_free(vx);
	gsl_vector_free(vy);
	gsl_vector_free(vdy);
	gsl_vector_free(c);
	gsl_vector_free(c1);
	gsl_matrix_free(S);
	gsl_matrix_free(S1);
return 0;	
}

double funs(int i, double x){
   switch(i){
   case 0: return log(x); break;
   case 1: return 1.0;   break;
   case 2: return x;     break;
   default: {fprintf(stderr,"funs: wrong i:%d",i); return NAN;}
   }
}

double funs1(int i, double x){
   switch(i){
   case 0: return x*x; break;
   case 1: return x;   break;
   case 2: return 1.0;     break;
   default: {fprintf(stderr,"funs: wrong i:%d",i); return NAN;}
   }
}

void matrix_print(gsl_matrix* A){   // prints elements of gsl matrix of size (size1, size2)
	for(int r=0;r<A->size1;r++){
		for(int c=0;c<A->size2;c++) printf(FMT,gsl_matrix_get(A,r,c));
		printf("\n");}
}




