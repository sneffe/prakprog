#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include"include.h"
#define RND ((double)rand()/RAND_MAX)  // Needed to get random matrix elements
#define FMT "%7.3f"
#include <time.h>

void matrix_print(gsl_matrix* A){   // prints elements of gsl matrix of size (size1, size2)
	for(int r=0;r<A->size1;r++){
		for(int c=0;c<A->size2;c++) printf(FMT,gsl_matrix_get(A,r,c));
		printf("\n");}
}

void vector_print(gsl_vector* v){   // prints elements of gsl matrix of size (size1, size2)
	for(int r=0;r<v->size;r++){
		printf("%g\n",gsl_vector_get(v,r));}
}

int main(){
	FILE * mystream;
	mystream = fopen("time.txt", "w+");
	
	int m_max = 150;
	int m_start = 5;
	int points = 20;
	int m_step = (m_max-m_start)/points;
	
	for(int m = m_start;m<=m_max;m=m+m_step){
	double t1 = clock();
	gsl_matrix* A = gsl_matrix_calloc(m,m);
	gsl_vector* e = gsl_vector_alloc(m);
	gsl_matrix* V = gsl_matrix_alloc(m,m);
	
	
	for(int i=0;i<m;i++){ 	// Create random symmetrical matrix A			
		for(int j=i;j<m;j++){
		gsl_matrix_set(A,i,j,RND);
		gsl_matrix_set(A,j,i,gsl_matrix_get(A,i,j));
		}
	}
	
	gsl_matrix* A_original = gsl_matrix_calloc(m,m);
	gsl_matrix_memcpy(A_original,A);
	

	if(m==m_start){
	printf("---First Exercise---\n");
	printf("The randomly generated matrix A is given by:\n");
	printf("A = \n");
	matrix_print(A);
	printf("\n");
}

	jacobi(A, e, V);

	gsl_matrix* VA = gsl_matrix_calloc(m,m); 
	gsl_matrix* D = gsl_matrix_calloc(m,m); 
	
	gsl_blas_dgemm(CblasTrans, CblasNoTrans , 1.0, V, A_original, 0.0, VA);
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans , 1.0, VA, V, 0.0, D);
	
if(m==m_start){
	
	printf("The matrix V containing the eigenvectors of A is found to be:\n");
	printf("V = \n");
	matrix_print(V);
	printf("\n");
	
	printf("The new matrix A_new is then found to be:\n");
	printf("A_new = \n");
	matrix_print(A);
	printf("\n");

	printf("We can check that V^T*A*V is equal to a diagonal matrix containing all the eigenvalues\n");
	printf("V^T*A*V = \n");
	matrix_print(D);
	printf("\n");
	
	printf("All the eigenvalues of A is found to be:\n");
	printf("e = \n");
	vector_print(e);
	printf("\n");
	
	printf("We can check that we have indeed found the correct eigenvectors and eigenvalues\n");
	gsl_vector* V_col = gsl_vector_alloc(V->size1);
	gsl_vector* b = gsl_vector_alloc(V->size1);
	
	for(int i=0;i<V->size2;i++){
	gsl_matrix_get_col(V_col,V,i);
	
	gsl_blas_dgemv(CblasNoTrans,1.0,A_original,V_col,0.0,b);
	gsl_vector_scale(b,1/gsl_vector_get(e,i));
	printf("(A*(V column %i))/e[%i] =\n",i,i);
	vector_print(b);
	printf("\n");
	}
	
	printf("We see that these vectors are indeed identical to the columns of V and hence the columns of V and the eigenvalues\n of the vector e are in fact the correct eigenvectors and eigenvalues of the matrix A\n\n");
	gsl_vector_free(V_col);
	gsl_vector_free(b);
}	
	
	gsl_matrix_free(A);
	gsl_vector_free(e);
	gsl_matrix_free(V);
	gsl_matrix_free(VA);
	gsl_matrix_free(D);
	gsl_matrix_free(A_original);

	double t2 = clock();
	
	fprintf(mystream, "%i %g\n",m,(t2-t1)/CLOCKS_PER_SEC);
	}
	fclose(mystream);

	printf("---Second Exercise---\n");
	int m = 8;
	int k = 3;
	gsl_matrix* A = gsl_matrix_calloc(m,m);	
	gsl_matrix* A_original = gsl_matrix_calloc(m,m);	
	gsl_vector* e = gsl_vector_alloc(m);
	gsl_matrix* V = gsl_matrix_alloc(m,m);
	
	for(int i=0;i<m;i++){ 	// Create random symmetrical matrix A			
		for(int j=i;j<m;j++){
		gsl_matrix_set(A,i,j,RND);
		gsl_matrix_set(A,j,i,gsl_matrix_get(A,i,j));
		}
	}
	gsl_matrix_memcpy(A_original,A);
	printf("A = \n");
	matrix_print(A);
	printf("\n");

	jacobik(A,e,V,k);
	
	printf("A_new = \n");
	matrix_print(A);
	printf("\n");

	printf("V = \n");
	matrix_print(V);
	printf("\n");

	gsl_matrix* VA = gsl_matrix_calloc(m,m); 
	gsl_matrix* D = gsl_matrix_calloc(m,m); 
	
	gsl_blas_dgemm(CblasTrans, CblasNoTrans , 1.0, V, A_original, 0.0, VA);
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans , 1.0, VA, V, 0.0, D);

	printf("V^T*A*V = \n");
	matrix_print(D);
	printf("\n");

	printf("e = \n");
	vector_print(e);
	printf("\n");


	gsl_matrix_free(A);
	gsl_matrix_free(V);
	gsl_vector_free(e);
	gsl_matrix_free(VA);
	gsl_matrix_free(D);
	return 0;
}