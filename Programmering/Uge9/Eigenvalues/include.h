#ifndef HAVE_QR
#define HAVE_QR

int jacobi(gsl_matrix* A, gsl_vector* e, gsl_matrix* V);
int jacobik(gsl_matrix* A, gsl_vector* e, gsl_matrix* V,int k);
#endif

