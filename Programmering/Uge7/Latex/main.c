#include<stdio.h>
#include<math.h>
#include<stdlib.h>

double DE_solver(double x);
int main(int argc, char **argv){
	
if(argc != 4){printf("The function takes 3 Input Parameters: The boundaries a and b and the step size dx");}

else{	
	double a = atof(argv[1]);
	double b = atof(argv[2]);
	double dx = atof(argv[3]);
	
	for(double x = a; x<=b; x+=dx){
		printf("%g\t%g\t%g\n",x, DE_solver(x), erf(x));
	}

}
return 0;
}

