#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>
#include<math.h>

int DE(double x, const double y[], double dydt[], void *params)
{
	dydt[0] = 2.0/(sqrt(M_PI))*exp(-x*x);
	return GSL_SUCCESS;
}


double DE_solver(double x)
{
	gsl_odeiv2_system sys;
	sys.function = DE;
	sys.jacobian = NULL;
	sys.dimension = 1;
	sys.params = NULL;

	gsl_odeiv2_driver *driver;
	double hstart = 0.1, abs = 1e-5, eps = 1e-5;
	driver = gsl_odeiv2_driver_alloc_y_new(&sys, gsl_odeiv2_step_rkf45, hstart, abs, eps);

	double x0 = -3;
	double y[] = {-1};
	gsl_odeiv2_driver_apply(driver, &x0, x, y);

	gsl_odeiv2_driver_free(driver);
	return y[0];
}
