#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<math.h>
#include"qr.h"

void newton_with_jacobian(void f(gsl_vector* z, gsl_vector* fz, gsl_matrix* J, int f_n),gsl_vector* x, double epsilon, int f_n){
	int n = x->size;
	
	gsl_matrix* J = gsl_matrix_alloc(n,n); // Declare all matrices and vectors
	gsl_matrix* R = gsl_matrix_alloc(n,n);
	gsl_vector* fz = gsl_vector_alloc(n);
	gsl_vector* fz_minus = gsl_vector_alloc(n);
	gsl_vector* df = gsl_vector_alloc(n);
	gsl_vector* Dx = gsl_vector_alloc(n);
	gsl_vector* Dx_lambda = gsl_vector_alloc(n);
	gsl_vector* x_step = gsl_vector_alloc(n);
	gsl_vector* fz_step = gsl_vector_alloc(n);
	gsl_matrix* J_step = gsl_matrix_alloc(n,n);
	
	int iterations = 1; // counter for the number of iterations. Increases by 1 every loop
do{
	f(x, fz, J, f_n); //calculate function values fz and jacobian matrix J
	gsl_vector_memcpy(fz_minus,fz);
	gsl_vector_scale(fz_minus,-1.0);
	qrdec(J,R); // QR decompose  J into QR. J is now replaced with Q
	qrsolve(J,R,fz_minus,Dx); //Solve the linear equation for Delta x
	
	double lambda = 1; // Starting value for the lambda parameter
	gsl_vector_memcpy(Dx_lambda,Dx); // Make copy of Dx
	gsl_vector_memcpy(x_step,x);
	gsl_vector_add(x_step,Dx);
	f(x_step, fz_step, J_step, f_n);
	
	double criterion = gsl_blas_dnrm2(fz_step);
	double check1 = (1-lambda/2)*gsl_blas_dnrm2(fz);
	double check2 = 1.0/64.0;

	while(criterion > check1 && lambda > check2){ // find sufficiently small lambda Criterion does not change value
		
		lambda = lambda/2;
		gsl_vector_scale(Dx_lambda, lambda);
	    gsl_vector_memcpy(x_step,x); // Turn x_step into x once again
		gsl_vector_add(x_step,Dx_lambda);
		f(x_step, fz_step, J_step, f_n);
		criterion = gsl_blas_dnrm2(fz_step);
		check1 = (1-lambda/2)*gsl_blas_dnrm2(fz);
	}
	
	gsl_vector_scale(Dx,lambda); // Dx --> lambda*Dx

	gsl_vector_add(x,Dx);	//take step with the selected lambda
	iterations++;
	f(x, fz, J, f_n);

	
} while(gsl_blas_dnrm2(fz)> epsilon);  // When we have that norm(fz) < epsilon we have reached an approximate solution
	printf("iterations: %i\n",iterations);
	
 // free all matrices and vectors from the memory
	gsl_matrix_free(J);
	gsl_matrix_free(R);
	gsl_vector_free(fz);
	gsl_vector_free(fz_minus);
	gsl_vector_free(df);
	gsl_vector_free(Dx);
	gsl_vector_free(Dx_lambda);
	gsl_vector_free(x_step);
	gsl_vector_free(fz_step);
	gsl_matrix_free(J_step);
}

void newton_with_jacobian_linesearch(void f(gsl_vector* z, gsl_vector* fz, gsl_matrix* J, int f_n),gsl_vector* x, double epsilon, int f_n){
	int n = x->size;
	
	gsl_matrix* J = gsl_matrix_alloc(n,n); // Declare all matrices and vectors
	gsl_matrix* R = gsl_matrix_alloc(n,n);
	gsl_vector* fz = gsl_vector_alloc(n);
	gsl_vector* fz_minus = gsl_vector_alloc(n);
	gsl_vector* df = gsl_vector_alloc(n);
	gsl_vector* Dx = gsl_vector_alloc(n);
	gsl_vector* Dx_lambda = gsl_vector_alloc(n);
	gsl_vector* x_step = gsl_vector_alloc(n);
	gsl_vector* fz_step = gsl_vector_alloc(n);
	gsl_matrix* J_step = gsl_matrix_alloc(n,n);
	
	int iterations = 1; // counter for the number of iterations. Increases by 1 every loop
do{
	f(x, fz, J, f_n); //calculate function values fz and jacobian matrix J
	gsl_vector_memcpy(fz_minus,fz);
	gsl_vector_scale(fz_minus,-1.0);
	qrdec(J,R); // QR decompose  J into QR. J is now replaced with Q
	qrsolve(J,R,fz_minus,Dx); //Solve the linear equation for Delta x
	
	double lambda = 1; // Starting value for the lambda parameter
	gsl_vector_memcpy(Dx_lambda,Dx); // Make copy of Dx
	gsl_vector_memcpy(x_step,x);
	gsl_vector_add(x_step,Dx);
	f(x_step, fz_step, J_step, f_n);
	
	double criterion = gsl_blas_dnrm2(fz_step);
	double check1 = (1-lambda/2)*gsl_blas_dnrm2(fz);
	double check2 = 1.0/64.0;

	while(criterion > check1 && lambda > check2){ 
		
		double g_trial = criterion*criterion/2;
		double g_0 = gsl_blas_dnrm2(fz)*gsl_blas_dnrm2(fz)/2; 
		double g_prime = -1.0*gsl_blas_dnrm2(fz)*gsl_blas_dnrm2(fz);
		double c = (g_trial-g_0-g_prime*lambda)/(lambda*lambda);
		
		lambda = -g_prime/(2.0*c);
		
		gsl_vector_scale(Dx_lambda, lambda);
	    gsl_vector_memcpy(x_step,x); // Turn x_step into x once again
		gsl_vector_add(x_step,Dx_lambda);
		f(x_step, fz_step, J_step, f_n);
		criterion = gsl_blas_dnrm2(fz_step);
		check1 = (1-lambda/2)*gsl_blas_dnrm2(fz);
	}
	
	gsl_vector_scale(Dx,lambda); // Dx --> lambda*Dx

	gsl_vector_add(x,Dx);	//take step with the selected lambda
	iterations++;
	f(x, fz, J, f_n);

} while(gsl_blas_dnrm2(fz)> epsilon);  // When we have that norm(fz) < epsilon we have reached an approximate solution
	printf("iterations: %i\n",iterations);
	
 // free all matrices and vectors from the memory
	gsl_matrix_free(J);
	gsl_matrix_free(R);
	gsl_vector_free(fz);
	gsl_vector_free(fz_minus);
	gsl_vector_free(df);
	gsl_vector_free(Dx);
	gsl_vector_free(Dx_lambda);
	gsl_vector_free(x_step);
	gsl_vector_free(fz_step);
	gsl_matrix_free(J_step);
}