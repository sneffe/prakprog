#include<stdio.h>
#include<assert.h>
#include<gsl/gsl_blas.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<math.h>
#define FMT "%7.3f"

double func(gsl_vector* x, int i, int f_n); 
void matrix_print(gsl_matrix* A);
void f(gsl_vector* z, gsl_vector* fz, gsl_matrix* J, int f_n);
void f_analytical(gsl_vector* x, gsl_vector* fx, gsl_matrix* J, int f_n);
void newton_with_jacobian(void f(gsl_vector* z, gsl_vector* fz, gsl_matrix* J, int f_n),gsl_vector* x, double epsilon, int f_n);
void newton_with_jacobian_linesearch(void f(gsl_vector* z, gsl_vector* fz, gsl_matrix* J, int f_n),gsl_vector* x, double epsilon, int f_n);

int main(){
	int n = 2; // Number of equations
	double epsilon = 1e-6; // Minimum uncertanty of the roots
	
	gsl_vector* x = gsl_vector_alloc(n);
	gsl_vector* fx = gsl_vector_alloc(n);
	gsl_matrix* J = gsl_matrix_alloc(n,n);
	
	gsl_vector_set(x,0,0.001); // Starting values for the first set of equations
	gsl_vector_set(x,1,10);
	
	printf("--- Exercise A ---\n\n");
	printf("Starting Point:\nx1 = %g\nx2 = %g\n\n",gsl_vector_get(x,0),gsl_vector_get(x,1));
	printf("Solution to the first set of equations\n");
	newton_with_jacobian(f_analytical, x, epsilon, 0); // Solve Root equation for the first system of equations
	printf("x1 = %g\nx2 = %g\n",gsl_vector_get(x,0),gsl_vector_get(x,1)); // Print Solution and prove that it is an approximate solution
	printf("f1(x1, x2) = %g\n",func(x,0,0));
	printf("f2(x1, x2) = %g\n\n",func(x,1,0));
	
	printf("Solution to the gradient of the rosenbrock function\n");
	newton_with_jacobian(f_analytical, x, epsilon, 1);// Solve Root equation for the Rosenbrock gradient. Use previous solution as starting point.
	printf("x1 = %g\nx2 = %g\n",gsl_vector_get(x,0),gsl_vector_get(x,1)); // Print Solution and prove that it is an approximate solution
	printf("f1(x1, x2) = %g\n",func(x,0,1));
	printf("f2(x1, x2) = %g\n",func(x,1,1));
	printf("The solution is inserted into the actual Rosenbrock function\n");
	printf("f_Rosenbrock = %g\n\n",func(x,2,1));
	
	gsl_vector_set(x,0,-3); // Prepare new starting values to ensure convergence at actual root
	gsl_vector_set(x,1,-3);
	printf("Solution to the gradient of the Himmelblau's function\n");
	newton_with_jacobian(f_analytical, x, epsilon, 2);// Solve Root equation for the Himmelblau gradient. Use previous solution as starting point.
	printf("x1 = %g\nx2 = %g\n",gsl_vector_get(x,0),gsl_vector_get(x,1)); // Print Solution and prove that it is an approximate solution
	printf("f1(x1, x2) = %g\n",func(x,0,2));
	printf("f2(x1, x2) = %g\n",func(x,1,2));
	printf("The solution is inserted into Himmelblau's function\n");
	printf("f_Himmelblau = %g\n\n",func(x,2,2));
	printf("All Solutions are within the selected error\n\n");
	
	gsl_vector_set(x,0,0.001); // Starting values for the first set of equations
	gsl_vector_set(x,1,10);
	
	printf("--- Exercise B ---\n\n");
	printf("Starting Point:\nx1 = %g\nx2 = %g\n",gsl_vector_get(x,0),gsl_vector_get(x,1));
	printf("The maximum error is set to: epsilon = %g\n\n",epsilon);
	
	printf("Solution to the first set of equations\n");
	newton_with_jacobian(f, x, epsilon, 0); // Solve Root equation for the first system of equations
	printf("x1 = %g\nx2 = %g\n",gsl_vector_get(x,0),gsl_vector_get(x,1)); // Print Solution and prove that it is an approximate solution
	printf("f1(x1, x2) = %g\n",func(x,0,0));
	printf("f2(x1, x2) = %g\n\n",func(x,1,0));
	
	printf("Solution to the gradient of the rosenbrock function\n");
	newton_with_jacobian(f, x, epsilon, 1);// Solve Root equation for the Rosenbrock gradient. Use previous solution as starting point.
	printf("x1 = %g\nx2 = %g\n",gsl_vector_get(x,0),gsl_vector_get(x,1)); // Print Solution and prove that it is an approximate solution
	printf("f1(x1, x2) = %g\n",func(x,0,1));
	printf("f2(x1, x2) = %g\n",func(x,1,1));
	printf("The solution is inserted into the actual Rosenbrock function\n");
	printf("f_Rosenbrock = %g\n\n",func(x,2,1));
	
	gsl_vector_set(x,0,-3); // Prepare new starting values to ensure convergence at actual root
	gsl_vector_set(x,1,-3);
	printf("Solution to the gradient of the Himmelblau's function\n");
	newton_with_jacobian(f, x, epsilon, 2);// Solve Root equation for the Himmelblau gradient. Use previous solution as starting point.
	printf("x1 = %g\nx2 = %g\n",gsl_vector_get(x,0),gsl_vector_get(x,1)); // Print Solution and prove that it is an approximate solution
	printf("f1(x1, x2) = %g\n",func(x,0,2));
	printf("f2(x1, x2) = %g\n",func(x,1,2));
	printf("The solution is inserted into Himmelblau's function\n");
	printf("f_Himmelblau = %g\n\n",func(x,2,2));
	printf("All Solutions are within the selected error\n\n");
	
	printf("--- Exercise C ---\n\n");
	gsl_vector_set(x,0,0.001); // Starting values for the first set of equations
	gsl_vector_set(x,1,10);
	printf("Starting Point:\nx1 = %g\nx2 = %g\n\n",gsl_vector_get(x,0),gsl_vector_get(x,1));
	
	printf("Solution to the first set of equations\n");
	newton_with_jacobian_linesearch(f, x, epsilon, 0);
	printf("x1 = %g\nx2 = %g\n",gsl_vector_get(x,0),gsl_vector_get(x,1)); // Print Solution and prove that it is an approximate solution
	printf("f1(x1, x2) = %g\n",func(x,0,0));
	printf("f2(x1, x2) = %g\n\n",func(x,1,0));
	
	printf("Solution to the first set of equations\n");
	newton_with_jacobian_linesearch(f, x, epsilon, 0); // Solve Root equation for the first system of equations
	printf("x1 = %g\nx2 = %g\n",gsl_vector_get(x,0),gsl_vector_get(x,1)); // Print Solution and prove that it is an approximate solution
	printf("f1(x1, x2) = %g\n",func(x,0,0));
	printf("f2(x1, x2) = %g\n\n",func(x,1,0));
	
	gsl_vector_set(x,0,-3); // Prepare new starting values to ensure convergence at actual root
	gsl_vector_set(x,1,-3);
	printf("Solution to the gradient of the Himmelblau's function\n");
	newton_with_jacobian_linesearch(f, x, epsilon, 2);// Solve Root equation for the Himmelblau gradient. Use previous solution as starting point.
	printf("x1 = %g\nx2 = %g\n",gsl_vector_get(x,0),gsl_vector_get(x,1)); // Print Solution and prove that it is an approximate solution
	printf("f1(x1, x2) = %g\n",func(x,0,2));
	printf("f2(x1, x2) = %g\n",func(x,1,2));
	printf("The solution is inserted into Himmelblau's function\n");
	printf("f_Himmelblau = %g\n\n",func(x,2,2));
	printf("All Solutions are within the selected error\n\n");
	
	gsl_vector_free(x);
	gsl_vector_free(fx);
	gsl_matrix_free(J);
	return 0;
}

void f_analytical(gsl_vector* x, gsl_vector* fx, gsl_matrix* J, int f_n){ // this function takes the vector x and calculates the function value and derivatives and jacobian matrix
	int n = x->size; //length of vector x
	if(n!=2){printf("The chosen function takes exactly two input parameters");}
	double A = 10000;
	for(int i =0; i<n; i++){
		gsl_vector_set(fx,i,func(x,i,f_n)); // Evaluate the i'th function for the given values in x
		}
		if(f_n == 0){
			gsl_matrix_set(J,0,0,A*gsl_vector_get(x,1));
			gsl_matrix_set(J,0,1,A*gsl_vector_get(x,0));
			gsl_matrix_set(J,1,0,-exp(-gsl_vector_get(x,0)));
			gsl_matrix_set(J,1,1,-exp(-gsl_vector_get(x,1)));
		}
		if(f_n == 1){
			gsl_matrix_set(J,0,0,2.0-400.0*gsl_vector_get(x,1)+1200*gsl_vector_get(x,0)*gsl_vector_get(x,0));
			gsl_matrix_set(J,0,1,-400*gsl_vector_get(x,0));
			gsl_matrix_set(J,1,0,-400*gsl_vector_get(x,0));
			gsl_matrix_set(J,1,1,200);
		}
		if(f_n == 2){
			gsl_matrix_set(J,0,0,12*gsl_vector_get(x,0)*gsl_vector_get(x,0)+4*gsl_vector_get(x,1)-42);
			gsl_matrix_set(J,0,1,4*gsl_vector_get(x,0) + 4*gsl_vector_get(x,1));
			gsl_matrix_set(J,1,0,4*gsl_vector_get(x,0) + 4*gsl_vector_get(x,1));
			gsl_matrix_set(J,1,1,4*gsl_vector_get(x,0)+12*gsl_vector_get(x,1)*gsl_vector_get(x,1)-26);
		}
	}
	
void f(gsl_vector* x, gsl_vector* fx, gsl_matrix* J, int f_n){ // this function takes the vector x and calculates the function value and derivatives and jacobian matrix
	int n = x->size; //length of vector x
	if(n!=2){printf("The chosen function takes exactly two input parameters");}
	double delta_x = 1e-6; // step size when finding partial derivatives
	gsl_vector* x_k = gsl_vector_alloc(n);
	gsl_vector_memcpy(x_k, x);
	
	for(int i =0; i<n; i++){
		gsl_vector_set(fx,i,func(x,i,f_n)); // Evaluate the i'th function for the given values in x
		for(int k=0;k<n; k++){
			gsl_vector_set(x_k,k,gsl_vector_get(x_k,k)+delta_x); // add delta_x to the k'th entry in the copy of x
			gsl_matrix_set(J,i,k,(func(x_k,i,f_n)-func(x,i,f_n))/delta_x);   //calculate the estimate of the (i,k) element in the jacobi matrix
			gsl_vector_set(x_k,k,gsl_vector_get(x_k,k)-delta_x); // subtract delta_x from the k'th entry in the copy of x
		}
	}
	gsl_vector_free(x_k);
}

double func(gsl_vector* x,int eq_n, int f_n){
	double A = 10000;
	if(f_n == 0){	
		if(eq_n==0){
	return A*gsl_vector_get(x,0)*gsl_vector_get(x,1)-1;
	}
	if(eq_n==1){
		return exp(-1*gsl_vector_get(x,0)) + exp(-1*gsl_vector_get(x,1))-1-1/A;
	}
	else{return NAN;}
}
if(f_n == 1){
	if(eq_n==0){
	return -2*(1-gsl_vector_get(x,0))-400*gsl_vector_get(x,0)*(gsl_vector_get(x,1)-gsl_vector_get(x,0)*gsl_vector_get(x,0))*(gsl_vector_get(x,1)-gsl_vector_get(x,0)*gsl_vector_get(x,0));
	}
	if(eq_n==1){
		return 200*(gsl_vector_get(x,1)-gsl_vector_get(x,0)*gsl_vector_get(x,0));
	}
	if(eq_n == 2){
			return (1-gsl_vector_get(x,0))*(1-gsl_vector_get(x,0))+100*(gsl_vector_get(x,1)-gsl_vector_get(x,0)*gsl_vector_get(x,0))*(gsl_vector_get(x,1)-gsl_vector_get(x,0)*gsl_vector_get(x,0));
	}
	else{return NAN;}
}

if(f_n == 2){
	if(eq_n==0){
	return 4*gsl_vector_get(x,0)*(gsl_vector_get(x,0)*gsl_vector_get(x,0)+gsl_vector_get(x,1)-11) + 2*(gsl_vector_get(x,0)+gsl_vector_get(x,1)*gsl_vector_get(x,1)-7);
	}
	if(eq_n==1){
		return 2*(gsl_vector_get(x,0)*gsl_vector_get(x,0)+gsl_vector_get(x,1)-11) + 4*gsl_vector_get(x,1)*(gsl_vector_get(x,0)+gsl_vector_get(x,1)*gsl_vector_get(x,1)-7);
	}
	
	if(eq_n == 2){
		return (gsl_vector_get(x,0)*gsl_vector_get(x,0)+gsl_vector_get(x,1)-11)*(gsl_vector_get(x,0)*gsl_vector_get(x,0)+gsl_vector_get(x,1)-11) + (gsl_vector_get(x,0)+gsl_vector_get(x,1)*gsl_vector_get(x,1)-7)*(gsl_vector_get(x,0)+gsl_vector_get(x,1)*gsl_vector_get(x,1)-7);
	}

	else{return NAN;}
}

else{return NAN;}
}

void matrix_print(gsl_matrix* A){   // prints elements of gsl matrix of size (size1, size2)
	for(int r=0;r<A->size1;r++){
		for(int c=0;c<A->size2;c++) printf(FMT,gsl_matrix_get(A,r,c));
		printf("\n");}
}

