
#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<math.h>
#define EPS (1.0/1048576)

int qnewton(void f(gsl_vector* z, gsl_vector* fz, gsl_vector* dfz, gsl_matrix* H, int f_n), gsl_vector* x, double epsilon, int f_n){
	
	int N = x->size; // Dimension of problem
	int nsteps = 0;
	int nbad = 0;
	int ngood = 0;
	
	gsl_vector* phi = gsl_vector_alloc(1);
	gsl_vector* dphi = gsl_vector_alloc(N);
	gsl_vector* Dx = gsl_vector_alloc(N);
	gsl_vector* z = gsl_vector_alloc(N);
	gsl_vector* phi_z = gsl_vector_alloc(N);
	gsl_matrix* H = gsl_matrix_alloc(N,N); // unused but enters in function call
	gsl_vector* dphi_z = gsl_vector_alloc(N);
	gsl_vector* y = gsl_vector_alloc(N);
	gsl_vector* u = gsl_vector_alloc(N);
	gsl_matrix* B = gsl_matrix_alloc(N,N);
	
	gsl_matrix_set_identity(B); // set B to the identity matrix of size N
	
	f(x, phi, dphi, H, f_n); // calculate function values, gradient and Hessian matrix
	double fx = gsl_vector_get(phi,0);
	double fz;
	
	while(nsteps < 1000){
		nsteps++;
		double lambda = 1; // initial value of the step scale parameter
		gsl_blas_dgemv(CblasNoTrans,-1,B,dphi,0,Dx); //eq. 6
		
		if(gsl_blas_dnrm2(Dx)<EPS*gsl_blas_dnrm2(x)){
			fprintf(stderr,"qnewton: |Dx|<EPS*|x|\n");
			break;
		}
		if(gsl_blas_dnrm2(dphi)<epsilon){
			fprintf(stderr,"qnewton: |grad|<acc\n");
			break;
		}
		
		while(1){
			gsl_vector_memcpy(z,x);
			gsl_vector_add(z,Dx);
			f(z,phi_z,dphi_z,H,f_n); // evaluate phi(z) and grad(phi(z))
			fz = gsl_vector_get(phi_z,0); // evaluate phi(z)
			
			double sTg;
			gsl_blas_ddot(Dx,dphi,&sTg);
			
			if(fz<fx+0.01*sTg){
				ngood++;
				break;
			}
			if(lambda<EPS){ 
				nbad++;
				break;
			}
			
			lambda = 0.5*lambda; // choose smaller lambda
			gsl_vector_scale(Dx,0.5); //update step Dx
		}
		
		f(z,phi_z, dphi_z,H, f_n);  
		gsl_vector_memcpy(y,dphi_z);     // y = grad(phi(z))
		gsl_blas_daxpy(-1,dphi,y); // y = grad(phi(z)) - grad(phi(x))
		gsl_vector_memcpy(u,Dx);
		gsl_blas_dgemv(CblasNoTrans,-1,B,y,1,u); // u = s-By
		
		double sTy;
		double uTy;
		
		gsl_blas_ddot(Dx,y, &sTy);
		if(fabs(sTy)>1e-12){
			gsl_blas_ddot(u,y,&uTy);
			double gamma = uTy/2/sTy;
			gsl_blas_daxpy(-gamma,Dx,u);
			gsl_blas_dger(1.0/sTy,u,Dx,B);
			gsl_blas_dger(1.0/sTy,Dx,u,B);
		}
		gsl_vector_memcpy(x,z);
		gsl_vector_memcpy(dphi,dphi_z); // grad(phi(x)) = grad(phi(z))
		fx = fz;
	}
	gsl_vector_free(phi);
	gsl_vector_free(dphi);
	gsl_vector_free(phi_z);
	gsl_vector_free(dphi_z);
	gsl_vector_free(Dx);
	gsl_vector_free(z);
	gsl_vector_free(y);
	gsl_vector_free(u);
	gsl_matrix_free(B);
	gsl_matrix_free(H);
	
	//fprintf(stdout,"qnewton: iterations=%i ngood=%i nbad=%i fx=%.1e\n",nsteps,ngood,nbad,fx);
return nsteps;
}