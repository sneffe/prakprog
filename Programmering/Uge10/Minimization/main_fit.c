#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<math.h>

void f(gsl_vector* z, gsl_vector* fz, gsl_vector* dfz, gsl_matrix* H, int f_n); // returns least square function and its derivatives
int qnewton(void f(gsl_vector* z, gsl_vector* fz, gsl_vector* dfz, gsl_matrix* H, int f_n), gsl_vector* x, double epsilon, int f_n);


int main(){
	
	double t[] = {0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77}; // measured time
	double y[] = {4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46}; // measured activity
	double e[] = {0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24}; // error on value
	int N = sizeof(t)/sizeof(t[0]);
	
	double epsilon = 1e-6;
	gsl_vector* X = gsl_vector_alloc(3); // three fitting parameters
	double A_guess = 5;
	double T_guess = 2;
	double B_guess = 0;
	
	gsl_vector_set(X,0,A_guess);
	gsl_vector_set(X,1,T_guess);
	gsl_vector_set(X,2,B_guess);
	
	qnewton(f,X,epsilon,0); // f_n = 0 is unused but is a relic of previous exercises
	
	
	FILE * datastream; //Open a data file and print the data points along with the fitted point into it
	datastream = fopen("data.txt", "w+");
	for(int i = 0;i<N;i++){
	fprintf(datastream,"%g %g %g %g\n", t[i],y[i],e[i],gsl_vector_get(X,0)*exp(-t[i]/gsl_vector_get(X,1))+gsl_vector_get(X,2));
	}
	fclose(datastream);
	
	FILE * fitstream; //Open a data file and print the data points along with the fitted point into it
	fitstream = fopen("fit.txt", "w+");
	for(double t = 0;t<10;t+=0.01){
	fprintf(fitstream,"%g %g\n", t,gsl_vector_get(X,0)*exp(-t/gsl_vector_get(X,1))+gsl_vector_get(X,2));
	}
	fclose(fitstream);
	
	printf("Obtained fitting parameters:\nA = %g\nT = %g\nB = %g\n",gsl_vector_get(X,0),gsl_vector_get(X,1),gsl_vector_get(X,2));
	
	gsl_vector_free(X);
	return 0;
}


void f(gsl_vector* z, gsl_vector* fz, gsl_vector* dfz, gsl_matrix* H, int f_n){
	double t[] = {0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77}; // measured time
	double y[] = {4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46}; // measured activity
	double e[] = {0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24}; // error on value
	int N = sizeof(t)/sizeof(t[0]);
	
	double A = gsl_vector_get(z,0);
	double T = gsl_vector_get(z,1);
	double B = gsl_vector_get(z,2);
	
	double fz_double = 0;
	double dfA = 0;
	double dfT = 0;
	double dfB = 0;
	
	for(int i = 0; i<N; i++){
		fz_double += pow((A*exp(-t[i]/T)+B-y[i]),2)/(e[i]*e[i]);
		dfA += 2*exp(-t[i]/T)*(A*exp(-t[i]/T)+B-y[i])/(e[i]*e[i]);
		dfT += 2*A*t[i]/(T*T)*exp(-t[i]/T)*(A*exp(-t[i]/T)+B-y[i])/(e[i]*e[i]);
		dfB += 2*(A*exp(-t[i]/T)+B-y[i])/(e[i]*e[i]);
	}
	
	gsl_vector_set(fz,0,fz_double);
	gsl_vector_set(dfz,0,dfA);
	gsl_vector_set(dfz,1,dfT);
	gsl_vector_set(dfz,2,dfB);
}