#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<math.h>

void f_analytical(gsl_vector* x, gsl_vector* phi, gsl_vector* df, gsl_matrix* H,int f_n); // function containing the two functions used f_n = 0: Rosenbrock Valley Function and f_n = 1: Himmelblau's function
void newton(void f(gsl_vector* x, gsl_vector* phi, gsl_vector* df, gsl_matrix* H, int f_n), gsl_vector* x_start, double epsilon, int f_n);
int qnewton(void f(gsl_vector* z, gsl_vector* phi, gsl_vector* df, gsl_matrix* H, int f_n), gsl_vector* x, double epsilon, int f_n);

int main(){
	int N = 2; //dimension of the problem
	double epsilon = 1e-6; // maximum error
	
	gsl_vector* x = gsl_vector_alloc(N); // contains starting values
	gsl_vector* phi = gsl_vector_alloc(1);
	gsl_vector* df = gsl_vector_alloc(N);
	gsl_matrix* H = gsl_matrix_alloc(N,N);
	
	gsl_vector_set(x,0,4); // Starting values
	gsl_vector_set(x,1,2);
	
	printf("--- Minimization ---\n");
	printf("Maximum Error of solutions to minima epsilon: %g \n\n", epsilon);
	printf("-- Exercise A --\n");
	printf("Rosenbrock Function\n");
	printf("x_start = \n");
	printf("%g\n", gsl_vector_get(x,0));
	printf("%g\n\n", gsl_vector_get(x,1));
	
	newton(f_analytical,x,epsilon,0);
	
	printf("x_min = \n");
	printf("%g\n", gsl_vector_get(x,0));
	printf("%g\n\n", gsl_vector_get(x,1));
	
	printf("Himmelblau Function\n");
	gsl_vector_set(x,0,-3); // Starting values
	gsl_vector_set(x,1,3);
	printf("x_start = \n");
	printf("%g\n", gsl_vector_get(x,0));
	printf("%g\n\n", gsl_vector_get(x,1));
	
	newton(f_analytical,x,epsilon,1);
	
	printf("x_min = \n");
	printf("%g\n", gsl_vector_get(x,0));
	printf("%g\n\n", gsl_vector_get(x,1));
	
	printf("-- Exercise B --\n");
	gsl_vector_set(x,0,4); // Starting values
	gsl_vector_set(x,1,2);
	
	printf("Rosenbrock Function\n");
	printf("x_start = \n");
	printf("%g\n", gsl_vector_get(x,0));
	printf("%g\n\n", gsl_vector_get(x,1));
	
	int nsteps = qnewton(f_analytical,x,epsilon,0);
	printf("iterations = %i\n",nsteps);
	printf("x_min = \n");
	printf("%g\n", gsl_vector_get(x,0));
	printf("%g\n\n", gsl_vector_get(x,1));
	
	printf("Himmelblau Function\n");
	gsl_vector_set(x,0,-3); // Starting values
	gsl_vector_set(x,1,3);
	printf("x_start = \n");
	printf("%g\n", gsl_vector_get(x,0));
	printf("%g\n\n", gsl_vector_get(x,1));
	
	nsteps = qnewton(f_analytical,x,epsilon,1);
	printf("iterations = %i\n",nsteps);
	printf("x_min = \n");
	printf("%g\n", gsl_vector_get(x,0));
	printf("%g\n\n\n", gsl_vector_get(x,1));
	
	printf("Clearly the quasi-newton method with Broyden's Update is an improvement\n\n");

	gsl_vector_free(x);
	gsl_vector_free(phi);
	gsl_vector_free(df);
	gsl_matrix_free(H);
	
	return 0;
}

void f_analytical(gsl_vector* x,gsl_vector* phi, gsl_vector* df, gsl_matrix* H, int f_n){
	double X = gsl_vector_get(x,0);
	double Y = gsl_vector_get(x,1);
	
	if(f_n == 0){
		gsl_vector_set(phi,0,(1-X)*(1-X) + 100*(Y-X*X)*(Y-X*X));
		
		gsl_vector_set(df,0,-2*(1-X)-400*X*(Y-X*X));
		gsl_vector_set(df,1,200*(Y-X*X));
	
		gsl_matrix_set(H,0,0, 2 + 1200*X*X -400*Y);
		gsl_matrix_set(H,1,0,-400);
		gsl_matrix_set(H,0,1,-400);
		gsl_matrix_set(H,1,1,200);
	}
	
	if(f_n == 1){
		gsl_vector_set(phi,0,(X*X+Y-11)*(X*X+Y-11) + (X+Y*Y-7)*(X+Y*Y-7));
		
		gsl_vector_set(df,0, 4*X*(X*X+Y-11) + 2*(X+Y*Y-7));
		gsl_vector_set(df,1, 2*(X*X+Y-11) + 4*Y*(X+Y*Y-7));
	
		gsl_matrix_set(H,0,0, 12*X*X + 4*Y - 42);
		gsl_matrix_set(H,1,0, 4*X + 2);
		gsl_matrix_set(H,0,1, 4*X + 2);
		gsl_matrix_set(H,1,1,4*X + 12*Y*Y - 26);
	}
	
}

		