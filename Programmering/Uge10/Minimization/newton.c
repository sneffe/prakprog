#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<math.h>
#include"qr.h"

void newton(void f(gsl_vector* x, gsl_vector* phi, gsl_vector* df, gsl_matrix* H, int f_n), gsl_vector* x_start, double epsilon, int f_n){
	
	int N = x_start->size; // Dimension of problem
	int i = 1;
	double lambda = 1; // initial value of the step scale parameter
	double alpha = 1e-4;
	
	 // vector with a single entry with the function evaluated at a position x_start

	gsl_vector* phi = gsl_vector_alloc(1);
	gsl_vector* df = gsl_vector_alloc(N);
	gsl_matrix* H = gsl_matrix_alloc(N,N);
	gsl_vector* df_minus = gsl_vector_alloc(N);
	gsl_matrix* R = gsl_matrix_alloc(N,N);
	gsl_vector* Dx = gsl_vector_alloc(N);
	gsl_vector* s = gsl_vector_alloc(N);
	gsl_vector* f_s = gsl_vector_alloc(N);
	gsl_vector* df_s = gsl_vector_alloc(N);
	gsl_vector* x_s = gsl_vector_alloc(N);
	
	do{
		lambda = 1;
		f(x_start, phi, df ,H, f_n); 
		gsl_vector_memcpy(df_minus,df);
		gsl_vector_scale(df_minus,-1);
		qrdec(H,R);
		qrsolve(H,R,df_minus,Dx);
		gsl_vector_memcpy(s,Dx);
		gsl_vector_memcpy(x_s,x_start);
		gsl_vector_scale(s,lambda);
		gsl_vector_add(x_s,s);
		
		f(x_s, f_s, df_s ,H, f_n); 
		
		double beta = 0; // s^T*grad(phi)
		for(int i = 0; i<N; i++){
			beta += gsl_vector_get(s,i)*gsl_vector_get(df,i);
		}
		
		while(gsl_vector_get(f_s,0) > gsl_vector_get(phi,0) + alpha*beta && lambda > 1/64.0){			//Armijo Condition
			//printf("lambda = %g\n",lambda);
			lambda = lambda/2;
			gsl_vector_scale(s,lambda);
			gsl_vector_add(x_s,s);
			f(x_s, f_s, df_s ,H, f_n); 
			
			beta = 0;
			for(int i = 0; i<N; i++){
				beta += gsl_vector_get(s,i)*gsl_vector_get(df,i);
			}	
			
			gsl_vector_memcpy(s,Dx); //reset s
			gsl_vector_memcpy(x_s,x_start);  // reset x_s
		}
		
		gsl_vector_scale(s,lambda); // set s = s*lambda
		gsl_vector_add(x_start,s); // set x = x + s
		
		i += 1;
		//printf("norm(df) = %g\n",gsl_blas_dnrm2(df));
	}
	
	while(gsl_blas_dnrm2(df) > epsilon);
	
	printf("iterations = %i\n",i);
	gsl_vector_free(phi);
	gsl_vector_free(df);
	gsl_matrix_free(H);
	gsl_vector_free(df_minus);
	gsl_matrix_free(R);
	gsl_vector_free(Dx);
	gsl_vector_free(s);
	gsl_vector_free(f_s);
	gsl_vector_free(df_s);
	gsl_vector_free(x_s);
}


